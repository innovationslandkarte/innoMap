#! /bin/bash
bundle exec rake assets:clobber
bundle exec rake assets:precompile

bundle exec rake db:exists && bundle exec rake db:migrate || bundle exec rake db:setup

rm -rf /innomap/tmp/pids/server.pid
exec "$@"
