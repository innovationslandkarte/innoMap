FROM ruby:slim
MAINTAINER kjoscha@gmail.com

RUN apt-get update -qq && apt-get install -y \
	gnupg2 \
	curl
ADD https://dl.yarnpkg.com/debian/pubkey.gpg /tmp/yarn-pubkey.gpg
RUN apt-key add /tmp/yarn-pubkey.gpg && rm /tmp/yarn-pubkey.gpg
RUN echo "deb http://dl.yarnpkg.com/debian/ stable main" > /etc/apt/sources.list.d/yarn.list
RUN curl -sL https://deb.nodesource.com/setup_8.x |  bash -

RUN apt-get update -qq && apt-get install -y \
    yarn \
    imagemagick \
    build-essential \
    libpq-dev \
    imagemagick \
    libmagick++-dev \
    nodejs \
    git \
    supervisor


RUN mkdir /innomap
WORKDIR /innomap
COPY . /innomap

RUN bundle install
RUN gem install bundler-audit
RUN yarn global add phantomjs-prebuilt

VOLUME /innomap/public
