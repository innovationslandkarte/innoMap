class GeoScenesController < ApplicationController
  before_action :assure_writing_permission, except: %i[index info_button_list show_on_map new create]
  before_action :assure_reading_permission, only: %i[index info_button_list show_on_map changes]
  before_action :authorize, only: %i[create new]

  def changes
    @geo_scene = GeoScene.find(params[:id])
    info_button_ids = @geo_scene.sub_scenes.first.info_buttons.map(&:id)
    versions_of_scene = PaperTrail::Version.where(item_type: 'InfoButton', item_id: info_button_ids)
    @versions = InfoButtonLog.new(versions_of_scene.order(created_at: :desc).limit(100)).hash
  end

  def show_on_map
    @geo_scene = GeoScene.find(params[:id])
    @categories = @geo_scene.scene_categories
    @welcome = @geo_scene.welcome

    respond_to do |format|
      format.html
      format.json { render json: @geo_scene.json_with_content }
    end
  end

  def index
    @geo_scenes = GeoScene.all.order(:created_at)
  end

  def settings
    @geo_scene = GeoScene.find(params[:id])
    @sub_scenes = @geo_scene.sub_scenes
    @geo_scene_categories = @geo_scene.scene_categories
  end

  def info_button_list
    @geo_scene = GeoScene.find(params[:id])
    @sub_scenes = @geo_scene.sub_scenes
    @attributes = @geo_scene.content_attributes
  end

  def new
    @main_scene = MainScene.find(main_scene_id)
    @geo_scene = @main_scene.geo_scenes.build(
      latitude: params[:latitude],
      longitude: params[:longitude]
    )
    5.times { @geo_scene.content_attributes.build }
  end

  def create
    @main_scene = MainScene.find(main_scene_id)
    @geo_scene = GeoScene.new(geo_scene_params)
    in_modal_validation(@geo_scene, validate_content_attributes && @geo_scene.save)
  end

  def edit
    @geo_scene = GeoScene.find(params[:id])
    5.times { @geo_scene.content_attributes.build }
  end

  def update
    @geo_scene = GeoScene.find(params[:id])
    in_modal_validation(@geo_scene, validate_content_attributes && @geo_scene.update_attributes(geo_scene_params) && delete_if_empty)
  end

  def destroy
    @geo_scene = GeoScene.find(params[:id])
    @geo_scene.destroy
    respond_to do |format|
      format.json { render json: nil, status: :ok }
      format.html { redirect_to scene_overview_path }
    end
  end

  private

  def main_scene_id
    if params[:id]
      Scene.find(params[:id]).main_scene_id
    else
      params[:main_scene_id] || geo_scene_params[:main_scene_id]
    end
  end

  def delete_if_empty
    @geo_scene.content_attributes.where(name: '').destroy_all
  end

  def assure_reading_permission
    assure_reading_permission_for(GeoScene.find(params[:id]))
  end

  def assure_writing_permission
    assure_writing_permission_for(GeoScene.find(params[:id]))
  end

  def geo_scene_params
    params.require(:geo_scene).permit(
      :main_scene_id,
      :text,
      :welcome,
      :latitude,
      :longitude,
      :tile_layer,
      :public_contributions_allowed,
      :public,
      content_attributes_attributes: %i[id name info position options]
    )
  end
end
