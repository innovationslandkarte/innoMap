class PagesController < ApplicationController
  def index
  end

  def use_cases
  end

  def scene_overview
    if current_user
      @main_scenes = MainScene.all.order(:created_at).select { |scene| current_user.reading_permission_for?(scene) }
    else
      # @main_scenes = MainScene.where(public: true).order(:created_at)
      # @geo_scenes = GeoScene.where(public: true).order(:created_at)
      @main_scenes = MainScene.public_access.order(:created_at)
    end
  end

  def imprint
  end

  def privacy
  end
end
