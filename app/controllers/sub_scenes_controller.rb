class SubScenesController < ApplicationController
  before_action :assure_writing_permission, except: [:index]

  skip_before_filter :verify_authenticity_token

  def index
    @sub_scenes = scene.sub_scenes
    @scene_categories = scene.scene_categories
    @scene = scene
  end

  def new
    @sub_scene = scene.sub_scenes.build(
      latitude: params[:latitude],
      longitude: params[:longitude]
    )
  end

  def create
    @sub_scene = scene.sub_scenes.build(sub_scene_params)
    in_modal_validation(@sub_scene, @sub_scene.save)
  end

  def edit
    @sub_scene = SubScene.find(params[:id])
  end

  def update
    @sub_scene = SubScene.find(params[:id])
    in_modal_validation(@sub_scene, @sub_scene.update_attributes(sub_scene_params))
  end

  def destroy
    @sub_scene = SubScene.find(params[:id])
    @sub_scene.destroy
    respond_to do |format|
      format.json { render json: nil, status: :ok }
      format.html { redirect_to :back }
    end
  end

  def all_info_buttons
    sub_scene = SubScene.find(params[:id])
    @info_buttons = sub_scene.scene.info_buttons
  end

  private

  def scene
    if params[:geo_scene_id]
      GeoScene.find(params[:geo_scene_id])
    elsif params[:graphical_scene_id]
      GraphicalScene.find(params[:graphical_scene_id])
    elsif params[:id]
      SubScene.find(params[:id]).scene
    else
      Scene.find(sub_scene_params[:scene_id])
    end
  end

  def assure_writing_permission
    assure_writing_permission_for(scene)
  end

  def sub_scene_params
    params.require(:sub_scene).permit(
      :text,
      :latitude,
      :longitude,
      :scene_id,
      :icon,
      :icon_size,
      :remove_icon
    )
  end
end
