class AdminController < ApplicationController
  before_action :authorize_as_admin, only: [:log]

  def log
    lately_updated_info_buttons = InfoButton.where('updated_at >= ?', 1.week.ago)
    @info_buttons = lately_updated_info_buttons.map do |info_button|
      {
        main_scene: info_button.sub_scene.parent.main_scene&.text || '-',
        scene: info_button.sub_scene.parent.text,
        sub_scene: info_button.sub_scene.text,
        info_button: info_button.text,
        updated_at: info_button.updated_at,
      }
    end

    lately_updated_sub_scenes = SubScene.where('updated_at >= ?', 1.week.ago)
    @sub_scenes = lately_updated_sub_scenes.map do |sub_scene|
      {
        main_scene: sub_scene.parent.main_scene&.text || '-',
        scene: sub_scene.parent.text,
        sub_scene: sub_scene.text,
        updated_at: sub_scene.updated_at,
      }
    end

    lately_updated_scenes = Scene.where('updated_at >= ?', 1.week.ago)
    @scenes = lately_updated_scenes.map do |scene|
      {
        main_scene: scene.main_scene&.text || '-',
        scene: scene.text,
        updated_at: scene.updated_at,
      }
    end

    lately_updated_main_scenes = MainScene.where('updated_at >= ?', 1.week.ago)
    @main_scenes = lately_updated_main_scenes.map do |main_scene|
      {
        main_scene: main_scene&.text || '-',
        updated_at: main_scene.updated_at,
      }
    end
  end
end
