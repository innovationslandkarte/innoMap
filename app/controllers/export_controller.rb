class ExportController < ApplicationController
  def excel
    @scene = params[:model] == 'geo_scene' ? GeoScene.find(params[:id]) : Scene.find(params[:id])

    respond_to do |format|
      format.xls { headers["Content-Disposition"] = "attachment; filename=\"#{@scene.text}.xls\"" }
    end
  end

  def pdf
    if params[:ids_for_pdf_export].nil?
      render text: 'No items selected for PDF export!'
    else
      @info_buttons = InfoButton.find(params[:ids_for_pdf_export])
      @attributes = params[:attributes_for_pdf_export]
      @scene = @info_buttons.first.sub_scene.scene
      flavoured_pdf('export/pdf_body')
    end
  end

  def single_item_pdf
    @info_button = InfoButton.find(params[:id])
    @scene = @info_button.sub_scene.scene
    flavoured_pdf('export/single_item_pdf_body')
  end

  private

  def flavoured_pdf(pdf_body)
    if @scene.class.name == 'Scene' && @scene.main_scene.id == 21
      render_pdf(pdf_body, 'export/_header_muensterland', nil)
    elsif @scene.class.name == 'GeoScene' && @scene.id == 35
      render_pdf(pdf_body, 'export/GIZ/_header_giz', 'export/GIZ/_footer_giz')
    elsif @scene.class.name == 'GeoScene' && @scene.id == 36
      render_pdf(pdf_body, 'export/GIZ/_header_sutp', 'export/GIZ/_footer_sutp')
    elsif @scene.class.name == 'GeoScene' && @scene.id == 37
      render_pdf(pdf_body, 'export/GIZ/_header_gpsm', 'export/GIZ/_footer_gpsm')
    elsif @scene.class.name == 'GeoScene' && @scene.id == 38
      render_pdf(pdf_body, 'export/GIZ/_header_tumi', 'export/GIZ/_footer_tumi')
    else
      render_pdf(pdf_body, 'export/_header_default', nil)
    end
  end

  def render_pdf(file, header, footer)
    render pdf: 'pdf',
           file: file,
           margin: { top: 45, bottom: 45, left: 20, right: 20 },
           header: { content: render_to_string(header, layout: nil), spacing: 5 },
           footer: footer ? { content: render_to_string(footer, layout: nil), spacing: 5 } : nil
  end
end
