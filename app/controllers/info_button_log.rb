class InfoButtonLog
  def initialize(versions)
    @versions = versions
  end

  attr_accessor :versions

  def hash
    @versions.map do |version|
      hash = {}
      hash['name'] = version.item.text
      hash['changes'] = unfold(version)
      hash
    end
  end

  def unfold(version)
    hash = {}
    version.changeset.each do |attribute, change_array|
      from = change_array[0]
      to = change_array[1]
      if from.is_a?(Hash)
        from.each do |a, c|
          hash[a] = [c, to[a]] unless c == to[a]
        end
      else
        hash[attribute] = change_array
      end
    end
    hash
  end
end
