class GraphicalScenesController < ApplicationController
  before_action :assure_writing_permission, except: %i[show_on_map info_button_list index]

  skip_before_filter :verify_authenticity_token

  def show_on_map
    @scene = GraphicalScene.find(params[:id])
    @main_scene = @scene.main_scene
    @welcome = @scene.welcome
    @categories = @scene.scene_categories

    respond_to do |format|
      format.html
      format.json { render json: @scene.json_with_content }
    end
  end

  def index
    @scenes = GraphicalScene.all.order(:created_at)
  end

  def settings
    @scene = GraphicalScene.find(params[:id])
    @main_scene = @scene.main_scene
    @sub_scenes = @scene.sub_scenes
    @scene_categories = @scene.scene_categories
    @system_effect_chains = @scene.system_effect_chains
  end

  def info_button_list
    @scene = GraphicalScene.find(params[:id])
    @main_scene = @scene.main_scene
    @sub_scenes = @scene.sub_scenes
    @attributes = @scene.content_attributes
  end

  def new
    @main_scene = MainScene.find(main_scene_id)
    @scene = @main_scene.graphical_scenes.build(
      latitude: params[:latitude],
      longitude: params[:longitude]
    )
    5.times { @scene.content_attributes.build }
  end

  def create
    @main_scene = MainScene.find(main_scene_id)
    @scene = @main_scene.graphical_scenes.build(scene_params)
    in_modal_validation(@scene, validate_content_attributes(geo: false) && @scene.save)
  end

  def edit
    @scene = GraphicalScene.find(params[:id])
    5.times { @scene.content_attributes.build }
  end

  def update
    @scene = GraphicalScene.find(params[:id])
    in_modal_validation(@scene, validate_content_attributes(geo: false) && @scene.update_attributes(scene_params) && delete_if_empty)
  end

  def destroy
    @scene = GraphicalScene.find(params[:id])
    @scene.destroy
    respond_to do |format|
      format.json { render json: nil, status: :ok }
      format.html { redirect_to scene_overview_path }
    end
  end

  private

  def delete_if_empty
    @scene.content_attributes.where(name: '').destroy_all
  end

  def assure_writing_permission
    assure_writing_permission_for(MainScene.find(main_scene_id))
  end

  def main_scene_id
    if params[:id]
      Scene.find(params[:id]).main_scene_id
    else
      params[:main_scene_id] || scene_params[:main_scene_id]
    end
  end

  def scene_params
    params.require(:graphical_scene).permit(
      :text,
      :base_image,
      :base_image_overlay,
      :remove_base_image_overlay,
      :latitude,
      :longitude,
      :main_scene_id,
      :welcome,
      :icon,
      :icon_size,
      :remove_icon,
      :public_contributions_allowed,
      content_attributes_attributes: %i[id name info position options]
    )
  end
end
