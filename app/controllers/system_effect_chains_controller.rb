class SystemEffectChainsController < ApplicationController
  def new
    @scene = Scene.find(params[:scene_id])
    @scene_categories = @scene.scene_categories
    @system_effect_chain = SystemEffectChain.new
    5.times { @system_effect_chain.system_effects.build }
  end

  def edit
    @system_effect_chain = SystemEffectChain.find(params[:id])
    @scene = @system_effect_chain.scene
    @scene_categories = @scene.scene_categories
    5.times { @system_effect_chain.system_effects.build }
  end

  def update
    @system_effect_chain = SystemEffectChain.find(params[:id])
    in_modal_validation(@system_effect_chain, @system_effect_chain.update_attributes(system_effect_chain_params))
  end

  def create
    @system_effect_chain = SystemEffectChain.new(system_effect_chain_params)
    @system_effects = @system_effect_chain.system_effects
    in_modal_validation(@system_effect_chain, @system_effect_chain.save)
  end

  def destroy
    @system_effect_chain = SystemEffectChain.find(params[:id])
    @system_effect_chain.destroy
    respond_to do |format|
      format.json { render json: nil, status: :ok }
      format.html { redirect_to :back }
    end
  end

  def system_effect_chain_params
    params.require(:system_effect_chain).permit(
      :text, :scene_category_id,
      system_effects_attributes: %i[id text content latitude longitude position _destroy]
    )
  end
end
