class SceneCategoriesController < ApplicationController
  before_action :assure_writing_permission, except: [:index]

  def index
    @scene_categories = scene.scene_categories
  end

  def new
    @scene = scene
    @scene_category = scene.scene_categories.build
  end

  def create
    @scene = scene
    @scene_category = SceneCategory.new(scene_category_params)
    in_modal_validation(@scene_category, @scene_category.save)
  end

  def edit
    @scene = scene
    @scene_category = SceneCategory.find(params[:id])
  end

  def update
    @scene_category = SceneCategory.find(params[:id])
    in_modal_validation(@scene_category, @scene_category.update_attributes(scene_category_params))
  end

  def destroy
    @scene_category = SceneCategory.find(params[:id])
    @scene_category.destroy
    redirect_to :back
  end

  private

  def scene
    Scene.find(params[:graphical_scene_id] || params[:geo_scene_id] || scene_category_params[:scene_id])
  end

  def assure_writing_permission
    assure_writing_permission_for(scene)
  end

  def scene_category_params
    params.require(:scene_category).permit(
      :name, :color, :icon, :image, :image_overlay,
      :remove_image, :remove_image_overlay, :scene_id, :position, :explanation
    )
  end
end
