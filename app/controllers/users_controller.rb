class UsersController < ApplicationController
  before_action :find_main_scene
  before_action except: [:destroy] do
    @main_scene ? assure_owner_for(@main_scene) : authorize_as_admin
  end

  def index
    @admins = @main_scene ? nil : User.admins
    @users = @main_scene ? @main_scene.users : User.all - @admins
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    in_modal_validation(@user, @user.update_attributes(user_params.except(:main_scene_id)))
    flash[:success] = "User editiert: Passwort: #{@user.password}"
  end

  def new
    @user = User.new
    @password = SecureRandom.hex(4)
  end

  def create
    @user = User.new(user_params.except(:main_scene_id))

    if @user.save && (@main_scene ? @user.assign_permission_for(@main_scene) : true)
      flash[:success] = "User erfolgreich erstellt. Passwort: #{@user.password}"
      redirect_to @main_scene ? main_scene_users_path(@main_scene) : users_path
    else
      flash.now[:danger] = @user.errors.full_messages.to_sentence
      render :new
    end
  end

  def destroy
    @user = User.find(params[:id])
    if current_user.has_permission_to_destroy_user?(@user)
      @user.destroy
      redirect_to :back
    else
      flash[:danger] = 'You are not allowed to do this! Propably this user has permissions for any other maps.'
      redirect_to :back
    end
  end

  private

  def find_main_scene
    main_scene_id = params[:main_scene_id] || (params[:user] && user_params[:main_scene_id])
    @main_scene = MainScene.find(main_scene_id) if main_scene_id
  end

  def user_params
    params.require(:user).permit(
      :email, :password, :password_confirmation, :main_scene_id,
      main_scenes_users_attributes: %i[id user_id main_scene_id own edit _destroy]
    )
  end
end
