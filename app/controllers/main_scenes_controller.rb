class MainScenesController < ApplicationController
  before_action :assure_writing_permission, except: %i[index show_on_map new create]
  before_action :assure_reading_permission, only: %i[index show_on_map]
  before_action :authorize, only: %i[create new]

  def users
    @main_scene = MainScene.find(params[:id])
    @users = @main_scene.users
  end

  def show_on_map
    @main_scene = MainScene.find(params[:id])
    @welcome = @main_scene.welcome
    @categories = @main_scene.main_scene_categories

    respond_to do |format|
      format.html
      format.json { render json: @main_scene.json_with_content }
    end
  end

  def index
    @main_scenes = MainScene.all.order(:created_at)
  end

  def settings
    @main_scene = MainScene.find(params[:id])
    @scenes = @main_scene.scenes
    @main_scene_categories = @main_scene.main_scene_categories
  end

  def new
    @main_scene = MainScene.new
  end

  def create
    @main_scene = MainScene.new(main_scene_params)
    in_modal_validation(@main_scene, @main_scene.save && current_user.assign_permission_for(@main_scene, edit: true))
  end

  def edit
    @main_scene = MainScene.find(params[:id])
  end

  def update
    @main_scene = MainScene.find(params[:id])
    in_modal_validation(@main_scene, @main_scene.update_attributes(main_scene_params))
  end

  def destroy
    @main_scene = MainScene.find(params[:id])
    @main_scene.destroy
    redirect_to scene_overview_path
  end

  private

  def assure_reading_permission
    assure_reading_permission_for(MainScene.find(params[:id]))
  end

  def assure_writing_permission
    assure_writing_permission_for(MainScene.find(params[:id]))
  end

  def main_scene_params
    params.require(:main_scene).permit(:text, :welcome, :base_image, :public)
  end
end
