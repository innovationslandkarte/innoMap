class MainSceneCategoriesController < ApplicationController
  before_action :assure_writing_permission, except: [:index]

  def index
    @main_scene_categories = main_scene.main_scene_categories
  end

  def new
    @main_scene = main_scene
    @main_scene_category = main_scene.main_scene_categories.build
  end

  def create
    @main_scene = main_scene
    @main_scene_category = MainSceneCategory.new(main_scene_category_params)
    in_modal_validation(@main_scene_category, @main_scene_category.save)
  end

  def edit
    @main_scene = main_scene
    @main_scene_category = MainSceneCategory.find(params[:id])
  end

  def update
    @main_scene_category = MainSceneCategory.find(params[:id])
    in_modal_validation(@main_scene_category, @main_scene_category.update_attributes(main_scene_category_params))
  end

  def destroy
    @main_scene_category = MainSceneCategory.find(params[:id])
    @main_scene_category.destroy
    redirect_to :back
  end

  private

  def main_scene
    main_scene_id = params[:main_scene_id] || main_scene_category_params[:main_scene_id]
    MainScene.find(main_scene_id)
  end

  def assure_writing_permission
    assure_writing_permission_for(main_scene)
  end

  def main_scene_category_params
    params.require(:main_scene_category).permit(:name, :color, :icon, :image, :main_scene_id, :position, :explanation)
  end
end
