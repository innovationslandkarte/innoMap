class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  protect_from_forgery with: :null_session

  before_action :set_locale
  before_action :set_paper_trail_whodunnit

  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end
  helper_method :current_user

  def authorize
    return false if current_user
    redirect_to login_path
  end

  def authorize_as_admin
    return false if current_user&.admin?
    redirect_to login_path
  end

  def assure_owner_for(object)
    return false if current_user&.owner_of?(object)
    redirect_to login_path
  end

  def assure_writing_permission_for(object)
    return false if current_user&.writing_permission_for?(object)
    redirect_to login_path
  end

  def assure_reading_permission_for(object)
    return false if object.public? || current_user&.reading_permission_for?(object)
    redirect_to login_path
  end

  def in_modal_validation(object, condition)
    if condition
      respond_to do |format|
        format.json { render json: object, status: :created }
        format.html { redirect_to :back }
      end
    else
      respond_to do |format|
        format.json { render json: object.errors.full_messages.to_sentence, status: :unprocessable_entity }
        format.html do
          flash[:danger] = object.errors.full_messages.to_sentence
          redirect_to :back
        end
      end
    end
  end

  def validate_content_attributes(geo: true)
    object_params = geo ? params[:geo_scene] : params[:graphical_scene]
    return true if !object_params[:content_attributes_attributes]
    accessor_names = object_params[:content_attributes_attributes].map{ |k,v| ContentAttribute.make_accessor_name(v[:name]) }.reject(&:empty?)
    if accessor_names.uniq.count != accessor_names.count
      object = geo ? @geo_scene : @scene
      object.errors.add(:base, 'Please use unique content attribute names!')
      return false
    else
      return true
    end
  end

  private

  before_action :set_locale

  def browser_language
    language_header = request.env['HTTP_ACCEPT_LANGUAGE']
    language_header&.scan(/^[a-z]{2}/)&.first&.to_sym
  end

  def supported_browser_language
    browser_language if I18n.available_locales.include?(browser_language)
  end

  def set_locale
    I18n.locale = params[:locale] || supported_browser_language || I18n.default_locale
  end

  def default_url_options
    { locale: I18n.locale }
  end
end
