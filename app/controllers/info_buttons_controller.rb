# rubocop:disable Metrics/ClassLength
class InfoButtonsController < ApplicationController
  protect_from_forgery except: :new
  before_action :derive_parent
  before_action :assure_writing_permission, except: %i[index show_on_map new create]
  before_action :contributions_allowed?, only: %i[new create]

  def index
    @info_buttons = @sub_scene.info_buttons
    respond_to do |format|
      format.html
      format.json { render json: @info_buttons.map(&:geojson) }
    end
  end

  def new
    @logged_in_with_rights = current_user&.writing_permission_for?(@sub_scene.scene)
    @scene_categories = scene_categories
    @system_effects = @scene_categories.map(&:system_effects).flatten
    @info_button = @sub_scene.info_buttons.build(
      latitude: params[:latitude],
      longitude: params[:longitude]
    )
  end

  def create
    @scene_categories = scene_categories
    @info_button = @sub_scene.info_buttons.new
    in_modal_validation(
      @info_button,
      (current_user&.writing_permission_for?(@sub_scene.scene) || verify_recaptcha(model: @info_button)) &&
        @info_button.update(info_button_params) &&
        make_guest_point_invisible)
  end

  def create_category_marker
    @info_button = @sub_scene.info_buttons.new(info_button_params)
    @info_button.update(icon: @info_button.scene_category.icon)
    in_modal_validation(@info_button, @info_button.save)
  end

  def clone
    info_button_to_clone = InfoButton.find(params[:id])
    category_to_clone = info_button_to_clone.scene_category
    attributes = info_button_to_clone.attributes
    cleaned_attributes = attributes.except('id')

    sub_scene = SubScene.find(info_button_params[:sub_scene_id])
    scene_category = sub_scene.scene.scene_categories.find(category_to_clone.id)

    @info_button = scene_category.info_buttons.new(cleaned_attributes)
    @info_button.update_attributes(info_button_params)

    in_modal_validation(@info_button, @info_button.save)
  end

  def edit
    @logged_in_with_rights = current_user&.writing_permission_for?(@sub_scene.scene)
    @info_button = InfoButton.find(params[:id])
    @scene_categories = scene_categories
    @system_effects = @scene_categories.map(&:system_effects).flatten
  end

  def update
    @info_button = InfoButton.find(params[:id])
    in_modal_validation(@info_button, @info_button.update_attributes(info_button_params))
  end

  def destroy
    @info_button = InfoButton.find(params[:id])
    @info_button.destroy
    respond_to do |format|
      format.json { render json: nil, status: :ok }
      format.html { redirect_to :back }
    end
  end

  private

  def make_guest_point_invisible
    if current_user&.writing_permission_for?(@sub_scene.scene)
      # to pass validation
      true
    else
      @info_button.update!(visible: false)
    end
  end

  def scene_categories
    @sub_scene.scene.scene_categories
  end

  def derive_parent
    sub_scene_id = if params[:id]
                     InfoButton.find(params[:id]).sub_scene_id
                   else
                     params[:sub_scene_id] || params[:info_button][:sub_scene_id]
                   end
    @sub_scene ||= SubScene.find(sub_scene_id)
  end

  def contributions_allowed?
    redirect_to :back unless @sub_scene.scene.public_contributions_allowed ||
                             current_user&.writing_permission_for?(@sub_scene.scene)
  end

  def assure_writing_permission
    assure_writing_permission_for(@sub_scene.scene)
  end

  def info_button_params
    attributes = %i[text long_title latitude longitude icon icon_size remove_icon
                    sub_scene_id scene_category_id system_effect_id visible]
    attributes << @sub_scene.scene.content_attributes.map(&:accessor_name).map(&:to_sym)
    params.require(:info_button).permit(attributes)
  end
end
