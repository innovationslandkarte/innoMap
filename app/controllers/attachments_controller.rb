class AttachmentsController < ApplicationController
  skip_before_filter :verify_authenticity_token

  def upload
    @attachment = Attachment.new
    @attachment.image = params[:file]
    @attachment.save

    respond_to do |format|
      format.json { render json: { status: 'OK', link: @attachment.image.url } }
    end
  end
end
