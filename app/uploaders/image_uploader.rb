class ImageUploader < CarrierWave::Uploader::Base
  storage :file

  def filename
    "#{date}_#{random_key}.#{file.extension}" if original_filename
  end

  def date
    @date ||= Time.now.strftime('%Y-%m-%d')
  end

  def random_key
    @random_key ||= SecureRandom.hex
  end

  def store_dir
    if model.class < Scene
      'uploads/scene/'
    else
      "uploads/#{model.class.to_s.underscore}/"
    end
  end

  def width
    rmagick_instance.columns if file
  end

  def height
    rmagick_instance.rows if file
  end

  def ratio
    width.to_f / height.to_f
  end

  def rmagick_instance
    ::Magick::Image.read(file.file).first
  end

  # Do not allow to upload potentially malicious files
  def extension_whitelist
    %w(jpg jpeg png gif svg)
  end
end
