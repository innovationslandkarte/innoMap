//= require map/scene_and_main_scene_settings
//= require map/zoom
//= require map/main_scene_functions
//= require map/editor
//= require map/filters
//= require map/welcome_modal

// load javascript when coming from browser's back button
jQuery(window).bind("pageshow", function(event) {
    if (event.originalEvent.persisted) {
        window.location.reload()
    }
});
