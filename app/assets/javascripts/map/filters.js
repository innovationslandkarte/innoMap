jQuery(function() {
  var fitBounds = function() {
    var bounds = window.infoButtonLayer.getBounds();
    if (typeof bounds._northEast != 'undefined') {
      map.fitBounds(bounds, {
        paddingTopLeft: [100, 100],
        paddingBottomRight: [100, 100]
      });
    };
  };

  var input = jQuery('#feature-search');
  if (input.length) {
    input.keyup(function() {
      renderInfoButtons();
      fitBounds();
    });
  };

  jQuery('.text-filter-toggle').click(function(e) {
    e.stopPropagation();
    button = jQuery(this);
    button.toggleClass('active');
    button.siblings('.filter-toggle-indicator').toggleClass('active');
    button.siblings('#feature-search').toggleClass('active');
    renderInfoButtons();
  });

  jQuery('.category-toggle').each(function(index, toggle) {
    var button = jQuery(toggle);
    var categoryImage = jQuery(toggle).attr('image_url');
    if (categoryImage) {
      var categoryImageLayer = L.imageOverlay(categoryImage, window.bounds, { opacity: 0 }).addTo(map).bringToFront();
      button.addClass('with-image');
    };
    var categoryImageOverlay = jQuery(toggle).attr('image_overlay_url');
    if (categoryImageOverlay) {
      var categoryImageOverlayLayer = L.imageOverlay(categoryImageOverlay, window.bounds, { opacity: 0 }).addTo(map).bringToFront();
      button.addClass('with-image');
    };
    button.click(function(e) {
      e.stopPropagation();
      button.toggleClass('active');
      button.siblings('.filter-toggle-text').toggleClass('active');
      button.siblings('.filter-toggle-indicator').toggleClass('active');
      if (typeof whiteLayer !== 'undefined') {
        var anyActiveImage = jQuery('.filter-toggle.with-image.active').length > 0;
        anyActiveImage ? whiteLayer.setOpacity(0.4) : whiteLayer.setOpacity(0);
      }
      if (categoryImage) {
        if (button.hasClass('active')) {
          categoryImageLayer.setOpacity(1);
        } else {
          categoryImageLayer.setOpacity(0);
        };
      };
      if (categoryImageOverlayLayer) {
        if (button.hasClass('active')) {
          categoryImageOverlayLayer.setOpacity(1);
        } else {
          categoryImageOverlayLayer.setOpacity(0);
        };
      };
      if (window.currentSubSceneId != null) {
        renderInfoButtons();
        renderSystemEffectChains();
      };
      jQuery('.info-overlay').fadeOut();
      map.removeLayer(systemEffectInfoButtonLayer);
    });
    if (window.categoriesActive == true) {
      button.click();
    };
  });

  jQuery('.filter-toggle-text').click(function() {
    jQuery(this).siblings('.category-toggle').trigger('click');
  });

  jQuery('.activate-all-categories').click(function() {
    jQuery('.category-toggle').each(function(index, toggle) {
      var button = jQuery(toggle);
      if (!button.hasClass('active')) {
        button.trigger('click');
      };
    });
  });

  jQuery('.deactivate-all-categories').click(function() {
    jQuery('.category-toggle').each(function(index, toggle) {
      var button = jQuery(toggle);
      if (button.hasClass('active')) {
        button.trigger('click');
      };
    });
  });

  // bring all filter toggle text containers to same width
  var resizeCategoryToggles = function() {
    var maxWidth = 0;
    jQuery('.filter-toggle-text')
      .each(function() {
        var width = jQuery(this).width();
        maxWidth = (width > maxWidth) ? width : maxWidth;
      })
      .each(function() {
        // + 15 due to margin/padding
        jQuery(this).width(maxWidth + 15);
      });
  };

  jQuery.when(resizeCategoryToggles()).done(function() {
    jQuery('.filter-toggle-container').css('opacity', 100);
  });
});