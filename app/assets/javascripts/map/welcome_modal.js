jQuery(function() {
  var modal = jQuery('#welcome-modal');
  if (modal.length > 0) {
    jQuery('.show-welcome-modal').append("<i class='icon-info-circled open-modal'></i>");
  };

  jQuery('.show-welcome-modal').click(function() {
    modal.modal('show');
  });

  modal.modal('show');
});
