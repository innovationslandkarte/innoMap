jQuery(function() {
  jQuery('#map').each(function() {
    jQuery('.navigation-arrow').hide();

    enteringPage = true;

    var updateScenes = function() {
      map.removeLayer(sceneLayer);
      makeScenes(mainScene.scenes);
      window.sceneLayer.addTo(map);
    };

    var makeScenes = function(json) {
      window.sceneLayer = L.geoJson(json, {
        onEachFeature: function(feature, layer) {
          layer.on('click', function(e) {
            var latlng = feature.geometry.coordinates;
            if (jQuery('.editor-button').hasClass('active')) {
              if (feature.properties.type == 'GraphicalScene') {
                editController('graphical_scenes', feature.id);
              } else {
                editController('geo_scenes', feature.id);
              }
            } else {
              map.setView([latlng[1], latlng[0]], map.getZoom() + 0.5, {
                pan: { animate: true, duration: 1 },
                zoom: { animate: true, duration: 10 },
              });
              jQuery('.map').fadeOut(3000);
              if (feature.properties.type == 'GraphicalScene') {
                window.location.href = '/' + window.locale + '/sub/' + feature.id;
              } else {
                window.location.href = '/' + window.locale + '/gmap/' + feature.id;
              };
            };
          });
        },
        pointToLayer: function(feature, latlng) {
          if (feature.properties.icon != null) {
            var options = {
              className: 'image-icon',
              html: feature.properties.icon,
              iconSize: [feature.properties.icon_size, feature.properties.icon_size]
            }
          } else {
            var options = {
              className: 'text-button map-button sub-scene-button',
              html: feature.properties.text + "<i class='icon-forward'></i>",
            }
          };

          return L.marker(latlng, {
            icon: L.divIcon(options)
          });
        }
      });
    };

    // global since editor uses it
    updateContent = function() {
      jQuery.ajax({
        url: '/map/' + window.currentMainSceneId,
        dataType: 'json',
        success: function(json) {
          window.mainScene = json;
          updateScenes();
        }
      });
    };

    var resetMap = function() {
      updateContent();
      setDefaultView();
      jQuery('.navigation-container > .level-1').html(window.mainSceneName);
    };
    resetMap();
  });
});
