//= require map/pie_chart_cluster_marker
//= require ../spinner

jQuery(function() {
  jQuery('#map').each(function() {
    jQuery('.navigation-arrow').hide();

    enteringPage = true;

    MIN_ZOOM_FOR_TEXT_MARKERS = window.currentGeoSceneId != null ? 9 : 5;

    zoomLevel = map.getZoom();
    map.on('moveend', function() {
      var previousZoomLevel = zoomLevel;
      zoomLevel = map.getZoom();
      if (zoomLevel >= MIN_ZOOM_FOR_TEXT_MARKERS && previousZoomLevel < MIN_ZOOM_FOR_TEXT_MARKERS) {
        renderInfoButtons({ singleMarkerMode: false });
      } else if (zoomLevel < MIN_ZOOM_FOR_TEXT_MARKERS && previousZoomLevel >= MIN_ZOOM_FOR_TEXT_MARKERS) {
        renderInfoButtons({ singleMarkerMode: true });
      };
    });

    var diveIntoSubScene = function(feature, layer, withZoom) {
      jQuery('.text-filter-row').show();

      var props = feature.properties;
      // navigation
      jQuery('.navigation-container > .level-3').html(props.text).show();
      jQuery('.navigation-arrow-2').show();

      jQuery('.clone-button').show();
      jQuery('.add-category-marker-button').show();
      jQuery('#feature-search').fadeIn();
      window.currentSubSceneId = layer.feature.id;
      var latlng = feature.geometry.coordinates;
      if (withZoom) {
        map.setView([latlng[1], latlng[0]], map.getZoom() + 0.3, {
          pan: { animate: true, duration: 1 },
          zoom: { animate: true },
        });
      };
      map.removeLayer(subSceneLayer);
      renderInfoButtons();
      setDefaultView();
    };

    var makeSubScenes = function(json) {
      window.subSceneLayer = L.markerClusterGroup({
        disableClusteringAtZoom: 10,
        maxClusterRadius: window.SUB_SCENES_CLUSTER_RADIUS,
        showCoverageOnHover: false
      });

      var marker = L.geoJson(json, {
        onEachFeature: function(feature, layer) {
          if (json.length == 1 && enteringPage == true) {
            enteringPage = false;
            diveIntoSubScene(feature, layer, false);
          };

          layer.on('click', function(e) {
            if (jQuery('.editor-button').hasClass('active')) {
              editController('sub_scenes', feature.id);
            } else {
              diveIntoSubScene(feature, layer, true);
            };
          });
        },
        pointToLayer: function(feature, latlng) {

          if (feature.properties.icon != null) {
            var options = {
              className: 'image-icon',
              html: feature.properties.icon,
              iconSize: [feature.properties.icon_size, feature.properties.icon_size]
            }
          } else {
            var options = {
              className: 'text-button map-button sub-scene-button',
              html: feature.properties.text + "<i class='icon-forward'></i>",
            }
          };
          return L.marker(latlng, {
            icon: L.divIcon(options)
          });
        }
      });

      window.subSceneLayer.addLayer(marker);
    };

    var infoButtonHtml = function(feature) {
      if (feature.properties.recent_guest_contribution) {
        var transparent = 'not-confirmed';
      } else {
        var transparent;
      };
      return "<div info_button_id=" + feature.id + " class='map-button info-button " + transparent + "' style='background: " + feature.properties.color + "'>" + feature.properties.title + "<i class='icon-info-circled'></i></div>";
    };

    var infoButtonClick = function(feature) {
      if (jQuery('.editor-button').hasClass('active')) {
        editController('info_buttons', feature.id);
      } else {
        jQuery('.info-overlay').hide();
        jQuery('.info-overlay-header').html(feature.properties.longest_title).removeClass().addClass('info-overlay-header').css('background', feature.properties.color);
        jQuery('.info-overlay-content').html(feature.properties.content);
        if (feature.properties.recent_guest_contribution) {
          jQuery('.info-overlay-content').append("<div class='waiting-for-confirmation'><h3> Waiting for confirmation! </h3>This point is visible for 5 minutes from creation and then waits for confirmation by the admins...</div>");
        };
        jQuery('.info-overlay').css('border-left', '8px solid ' + feature.properties.color);
        jQuery('.info-overlay').fadeIn();
        setTimeout(function() { jQuery('.info-overlay').scrollTop(0) }, 1);
        initRSlides();
        if (map.getZoom() < MIN_ZOOM_FOR_TEXT_MARKERS) {
          map.setView([feature.geometry.coordinates[1], feature.geometry.coordinates[0]], MIN_ZOOM_FOR_TEXT_MARKERS);
        } else {
          map.panTo([feature.geometry.coordinates[1], feature.geometry.coordinates[0]], { easeLinearity: 0 });
        };
        var pdfLink = '<a class="btn btn-default icon-download pull-right" href="/export/single_item_pdf?id=' + feature.id + '"> PDF</a>';
        jQuery('.info-overlay-content').append(pdfLink);
      };
    };

    var makeInfoButtons = function(json, cloneInOverlay, singleMarkerMode) {
      var singleMarkerMode = singleMarkerMode;
      clusterGroup = L.markerClusterGroup({
        maxClusterRadius: window.INFO_CLUSTER_RADIUS,
        showCoverageOnHover: false,
        spiderfyDistanceMultiplier: 2,
        singleMarkerMode: window.SINGLE_MARKER_MODE && singleMarkerMode,
        spiderLegPolylineOptions: { weight: 5, color: 'red', opacity: 0 },
        iconCreateFunction: defineClusterIcon
      });

      var markers = L.geoJson(json, {
        onEachFeature: function(feature, layer) {
          if (cloneInOverlay) {
            var div = document.createElement('div');
            jQuery(div)
              .html(infoButtonHtml(feature))
              .appendTo('.info-overlay-content')
              .click(function() {
                infoButtonClick(feature);
              });
          };

          layer.on('click', function(e) {
            infoButtonClick(feature);
          });
        },
        pointToLayer: function(feature, latlng) {
          if (feature.properties.icon != null) {
            var options = {
              className: 'image-icon',
              html: feature.properties.icon,
              iconSize: [feature.properties.icon_size, feature.properties.icon_size]
            }
          } else {
            var options = {
              className: 'text-button',
              html: infoButtonHtml(feature),
            }
          };
          return L.marker(latlng, {
            icon: L.divIcon(options)
          });
        }
      });
      clusterGroup.addLayer(markers);
      window.infoButtonBounds = json.length > 0 ? markers.getBounds() : null;
      return clusterGroup;
    };

    var systemEffectClick = function(systemEffectChain, feature) {
      map.removeLayer(window.systemEffectInfoButtonLayer);
      if (jQuery('.editor-button').hasClass('active')) {
        editController('system_effect_chains', systemEffectChain.id);
      } else {
        jQuery('.info-overlay').hide();
        jQuery('.info-overlay-header').html(feature.properties.text).removeClass().addClass('info-overlay-header').css('background', systemEffectChain.properties.color);
        jQuery('.info-overlay').css('border-left', '8px solid ' + systemEffectChain.properties.color);
        jQuery('.info-overlay-content').html("<div class='system-effect-index-container'></div>");
        jQuery.each(systemEffectChain.systemEffects, function(id, systemEffect) {
          var indexButton = document.createElement('div');
          jQuery(indexButton)
            .addClass('system-effect-index')
            .attr('system_effect_id', systemEffect.id)
            .html(systemEffect.properties.position)
            .css('color', systemEffectChain.properties.color)
            .appendTo(jQuery('.system-effect-index-container'));

          if (systemEffect.properties.position == feature.properties.position) {
            jQuery(indexButton).css('font-size', '+=10').addClass('active');
          };

          jQuery(indexButton).on('click', function() {
            systemEffectClick(systemEffectChain, systemEffect);
          });
        });
        map.panTo([feature.properties.latitude, feature.properties.longitude], { easeLinearity: 0 });
        jQuery('.info-overlay-content').append(feature.properties.content);
        jQuery('.info-overlay').fadeIn();
        window.systemEffectInfoButtonLayer = makeInfoButtons(feature.infoButtons, true);
        window.systemEffectInfoButtonLayer.addTo(map);
      };
    };

    var makeSystemEffectChains = function(json) {
      window.systemEffectChainGroupLayer = L.layerGroup();
      jQuery.each(json, function(id, systemEffectChain) {
        L.geoJson(systemEffectChain.systemEffects, {
          onEachFeature: function(feature, layer) {
            layer.on('click', function(e) {
              systemEffectClick(systemEffectChain, feature);
            });
          },
          pointToLayer: function(feature, latlng) {
            return L.marker(latlng, {
              icon: L.divIcon({
                className: 'system-effect-button',
                html: "<div system_effect_id=" + feature.id + " style='color:" + systemEffectChain.properties.color + "'>" + feature.properties.position + "</div>",
              })
            });
          }
        }).addTo(systemEffectChainGroupLayer);
      });
    };

    renderInfoButtons = function(options) {
      map.removeLayer(infoButtonLayer);
      var o = options ? options : {};
      var singleMarkerMode = o.singleMarkerMode ? o.singleMarkerMode : map.getZoom() < MIN_ZOOM_FOR_TEXT_MARKERS;
      window.infoButtonLayer = makeInfoButtons(fromSearchInput(fromCategories(fromCurrentSubScene(window.scene.infoButtons))), false, singleMarkerMode);
      window.infoButtonLayer.addTo(map);
    };

    renderSubScenes = function() {
      map.removeLayer(subSceneLayer);
      makeSubScenes(scene.subScenes);
      window.subSceneLayer.addTo(map);
    };

    renderSystemEffectChains = function() {
      map.removeLayer(window.systemEffectChainGroupLayer);
      map.removeLayer(window.systemEffectInfoButtonLayer);
      makeSystemEffectChains(fromCategories(scene.systemEffectChains));
      window.systemEffectChainGroupLayer.addTo(map);
    };

    // global since editor uses it
    updateContent = function() {
      if (window.currentGeoSceneId != null) {
        var url = "/gmap/" + window.currentGeoSceneId;
      } else {
        var url = "/sub/" + window.currentSceneId;
      };

      showLoadingAnimation();

      jQuery.ajax({
        url: url,
        dataType: 'json',
        success: function(json) {
          window.scene = json;
        },
        complete: function() {
          renderSubScenes();
          renderInfoButtons();
          renderSystemEffectChains();
          hideLoadingAnimation();
          if (window.currentSubSceneId != null) {
            map.removeLayer(subSceneLayer);
          };
        }
      });
    };

    var fromCurrentSubScene = function(json) {
      var filteredJson = [];
      jQuery(json).each(function(id, feature) {
        if (feature.properties.sub_scene == window.currentSubSceneId) {
          filteredJson.push(feature);
        };
      });
      return filteredJson;
    };

    var fromCategories = function(json) {
      var filteredJson = [];
      jQuery('.filter-toggle').each(function(index, toggle) {
        var category = jQuery(toggle).attr('category');
        if (jQuery(toggle).hasClass('active')) {
          jQuery(json).each(function(id, feature) {
            if (feature.properties.category == category) {
              filteredJson.push(feature);
            };
          });
        };
      });
      return filteredJson;
    };

    var fromSearchInput = function(json) {
      if (jQuery('#feature-search').hasClass('active')) {
        var filter = jQuery("#feature-search").val();
        var filteredJson = [];
        jQuery(json).each(function(id, feature) {
          if (
            (feature.properties.longest_title.toLowerCase().indexOf(filter.toLowerCase()) >= 0) ||
            (feature.properties.title.toLowerCase().indexOf(filter.toLowerCase()) >= 0) ||
            (feature.properties.content.toLowerCase().indexOf(filter.toLowerCase()) >= 0)
          ) {
            filteredJson.push(feature);
          };
        });
        return filteredJson;
      } else {
        return json;
      };
    };

    jQuery('.info-overlay-close').click(function() {
      jQuery('.info-overlay').fadeOut();
      map.removeLayer(systemEffectInfoButtonLayer);
    });

    map.on('click', function() {
      jQuery('.info-overlay').fadeOut();
      map.removeLayer(systemEffectInfoButtonLayer);
    });

    var resetMap = function() {
      // navigation
      jQuery('.navigation-container > .level-3').hide();
      if (window.mainSceneName !== undefined) {
        jQuery('.navigation-arrow-1').show();
      };
      jQuery('.navigation-arrow-2').hide();

      jQuery('.add-category-marker-button').hide();
      jQuery('.clone-button').hide();
      jQuery('#feature-search').fadeOut().val('');

      // does also happen in updateContent(), but this way it is faster when going back from a subScene
      map.removeLayer(window.infoButtonLayer);

      updateContent();
      window.currentSubSceneId = null;
      setDefaultView();
      jQuery('.text-filter-row').hide();
      jQuery('.info-overlay').fadeOut();
    };
    resetMap();

    jQuery('#form-modal').on('show.bs.modal', function() {
      jQuery('.info-overlay').fadeOut();
    });

    // navigation
    jQuery('.navigation-container > .level-1')
      .html(window.mainSceneName)
      .click(function() {
        window.location.href = '/' + window.locale + '/map/' + window.mainSceneId;
      });

    jQuery('.navigation-container > .level-2')
      .html(window.sceneName)
      .click(function() {
        resetMap();
      });

    jQuery('#back-button').click(function() {
      if (window.currentSubSceneId) {
        resetMap();
      } else {
        window.location.href = '/' + window.locale + '/map/' + window.mainSceneId;
      }
    })
  });
});