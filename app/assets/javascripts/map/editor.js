jQuery(function() {
  jQuery('#map').each(function() {
    map.on('click', function(point) {
      // NEW CATEGORY BUTTON BUTTON
      if (window.currentSubSceneId != null && addCategoryMarkerButtons.hasClass('active')) {
        var button = jQuery('.add-category-marker-button.active');
        jQuery.ajax({
          url: '/info_buttons/create_category_marker',
          type: 'POST',
          dataType: 'json',
          data: {
            sub_scene_id:  window.currentSubSceneId,
            info_button: {
              text: button.attr('category_name'),
              latitude: point.latlng.lat,
              longitude: point.latlng.lng,
              scene_category_id: button.attr('category'),
            },
          },
        });
      } else if (window.infoButtonsToClone != null && window.currentSubSceneId != null) {
        jQuery.ajax({
          url: '/info_buttons/' + window.infoButtonsToClone + '/clone',
          type: 'POST',
          dataType: 'json',
          data: {
            info_button: {
              sub_scene_id: window.currentSubSceneId,
              latitude: point.latlng.lat,
              longitude: point.latlng.lng,
            },
          },
        });
      } else if (jQuery('.add-button').hasClass('active')) {
        var params = 'latitude=' + point.latlng.lat + '&longitude=' + point.latlng.lng + '&remote=true' + '&locale=' + window.locale;
        // NEW INFO BUTTON
        if (window.currentSubSceneId != null) {
          jQuery.ajax({ url: '/sub_scenes/' + window.currentSubSceneId + '/info_buttons/new?' + params });
        // NEW SCENE
        } else if (window.currentMainSceneId != null) {
            jQuery('#map-style-selection').modal('show');
            // SELECT WHICH MAP STYLE TO ADD
            jQuery('#select-graphical-map').click(function() {
              jQuery('#map-style-selection').modal('hide');
              jQuery.ajax({ url: '/main_scenes/' + window.currentMainSceneId + '/graphical_scenes/new?' + params });
            });
            jQuery('#select-geo-map').click(function() {
              jQuery('#map-style-selection').modal('hide');
              jQuery.ajax({ url: '/main_scenes/' + window.currentMainSceneId + '/geo_scenes/new?' + params });
            });
        // NEW GEOGRAPHIC SUB SCENE
        } else if (window.currentGeoSceneId != null) {
            jQuery.ajax({ url: "/geo_scenes/" + window.currentGeoSceneId + "/sub_scenes/new?" + params + "&geo_scene=true"});
        // NEW GRAPHICAL SUB SCENE
        } else if (window.currentSceneId != null) {
            jQuery.ajax({ url: "/graphical_scenes/" + window.currentSceneId + "/sub_scenes/new?" + params});
        };
      } else if (window.featureToBeMoved != null) {
        if      (window.featureToBeMoved['type'] == 'info_buttons') { var key = 'info_button' }
        else if (window.featureToBeMoved['type'] == 'sub_scenes') { var key = 'sub_scene' }
        else if (window.featureToBeMoved['type'] == 'graphical_scenes') { var key = 'graphical_scene' };
        var data = {};
        data[key] = {
          latitude: point.latlng.lat,
          longitude: point.latlng.lng,
        };
        jQuery.ajax({
          url: '/' + window.featureToBeMoved['type'] + '/' + window.featureToBeMoved['id'],
          type: 'PATCH',
          dataType: 'json',
          data: data,
          success: function() { updateContent(); }
        });
      };
    });

    window.editController = function(type, feature_id) {
      if (jQuery('.edit-button').hasClass('active')) {
        editFeature(type, feature_id);
      } else if (jQuery('.delete-button').hasClass('active')) {
        if (confirm('You really want to delete this?') == true) {
          deleteFeature(type, feature_id);
        };
      } else if (jQuery('.move-button').hasClass('active')) {
        moveFeature(type, feature_id);
      };
    };

    var editFeature = function(type, feature_id) {
      jQuery.ajax({ url: "/" + type + "/" + feature_id + '/edit' + '?remote=true' + '&locale=' + window.locale });
    };

    var deleteFeature = function(type, feature_id) {
      jQuery.ajax({
        url: "/" + type + "/" + feature_id,
        type: 'DELETE',
        success: function() { updateContent(); }
      });
    };

    var moveFeature = function(type, feature_id) {
      jQuery('.move-sign-on-cursor').addClass('feature-chosen');
      window.featureToBeMoved = {
        id: feature_id,
        type: type
      };
    };

    jQuery(document).ajaxComplete(function( event, xhr, settings ) {
      if (settings.type == ('POST' || 'DELETE')) {
        updateContent();
      };
    });

    //---------- BUTTONS ----------//
    jQuery(window).mousemove( function(e) {
      var navbarHeight = jQuery('.navbar:visible').height();
      mouseX = e.pageX;
      mouseY = e.pageY;
      jQuery('.sign-on-cursor').css({
        'top': mouseY - navbarHeight,
        'left': mouseX + 15
      });
    });

    var addCategoryMarkerButtons = jQuery('.add-category-marker-button');

    var leaveEditorMode = function() {
      window.featureToBeMoved = null;
      window.infoButtonsToClone = null;
      jQuery('.move-sign-on-cursor').removeClass('feature-chosen');
      jQuery('.editor-button').removeClass('active');
      jQuery('.sign-on-cursor').hide();
    };

    addCategoryMarkerButtons.on('click', function(event) {
      event.stopPropagation();
    });

    jQuery('.editor-button').click(function() {
      jQuery('#map').css('cursor', 'default');
      jQuery('.sign-on-cursor').hide();
      jQuery('.leave-editor').show();
      if (!jQuery(this).hasClass('active')) {
        jQuery('.editor-button').removeClass('active');
        jQuery(this).addClass('active');
      } else {
        leaveEditorMode();
      };
      if (jQuery('.add-category-marker-button').hasClass('active')) {
        jQuery('.add-sign-on-cursor').show();
      } else if (jQuery('.add-button').hasClass('active')) {
        jQuery('.add-sign-on-cursor').show();
      } else if (jQuery('.move-button').hasClass('active')) {
        jQuery('.move-sign-on-cursor').show();
      } else if (jQuery('.edit-button').hasClass('active')) {
        jQuery('.edit-sign-on-cursor').show();
      } else if (jQuery('.delete-button').hasClass('active')) {
        jQuery('.delete-sign-on-cursor').show();
      };
    });

    jQuery('#map').mouseenter( function() {
      jQuery('.sign-on-cursor').css('opacity', 1);
    });

    jQuery('#map').mouseleave( function() {
      jQuery('.sign-on-cursor').css('opacity', 0);
    });

    jQuery('.map-overlay').mouseenter( function() {
      jQuery('.sign-on-cursor').css('opacity', 0);
    });

    jQuery('.map-overlay').mouseleave( function() {
      jQuery('.sign-on-cursor').css('opacity', 1);
    });

    window.addEventListener("keydown", function (event) {
      if (event.defaultPrevented) {
        return; // Should do nothing if the key event was already consumed.
      }
      switch (event.key) {
        case "Escape":
          leaveEditorMode();
        break;
        default:
        return; // Quit when this doesn't handle the key event.
      }
      // Consume the event to avoid it being handled twice
      event.preventDefault();
    }, true);

    // INFO BUTTON CLONER
    jQuery('.clone-button').click(function() {
      if (window.currentSubSceneId && jQuery(this).hasClass('active')) {
        jQuery.ajax({  url: '/sub_scenes/' + window.currentSubSceneId + '/all_info_buttons' });
      };
    });

    jQuery(document).on('click', '.info_button_clone', function() {
      var id = jQuery(this).attr('info_button_clone_id');
      window.infoButtonsToClone = id;
      jQuery('.add-sign-on-cursor').show();
      jQuery('.modal').modal('hide');
    });

    jQuery(document).on('click', '.close-cloner_list', function() {
      jQuery('.clone-button').removeClass('active');
    });
  });
});
