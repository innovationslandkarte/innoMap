jQuery(function() {
  jQuery('#map').each(function() {

    window.SUB_SCENES_CLUSTER_RADIUS = 10;
    window.INFO_CLUSTER_RADIUS = 10;
    window.SINGLE_MARKER_MODE = false;

    window.bounds = L.latLngBounds([[0,0], [100, 100 * window.ratio]]);

    window.map = L.map('map', {
      crs: L.CRS.Simple,
      maxBounds: bounds,
      minZoom: 3,
      maxZoom: 6,
      zoomSnap: 0,
      zoomAnimationThreshold: 10,
    });

    map.on('drag', function() { map.panInsideBounds(bounds, { animate: false }); });

    window.base = L.imageOverlay(window.base, bounds).addTo(map);
    if (window.baseOverlay) {
      L.imageOverlay(window.baseOverlay, window.bounds).addTo(map).bringToFront();
    };
    window.whiteLayer = L.imageOverlay(window.whiteLayer, window.bounds, {opacity: 0}).addTo(map).bringToFront();

    window.currentSubSceneIdId = null;
    window.sceneJson = {};
    window.sceneLayer = {};
    window.infoButtonLayer = {};
    window.systemEffectInfoButtonLayer = {};
    window.subSceneLayer = {};
    window.systemEffectChainGroupLayer = {};

    // GLOBAL FUNCTIONS
    setDefaultView = function() {
      map.fitBounds(window.bounds, {
        padding: [-200, -200],
        pan: { animate: true , duration: 1 },
        zoom: { animate: true },
        animate: true,
      });
    };
  });
});
