jQuery(function() {
  if (window.currentGeoSceneId != null) {
    stepSize = 1;
  } else {
    stepSize = 0.5;
  };

  jQuery('.zoom-in').click(function(){
    map.doubleClickZoom.disable();
    setTimeout(function(){
      map.doubleClickZoom.enable();
    }, 500);
    map.setZoom(map.getZoom() + stepSize);
  });
  jQuery('.zoom-out').click(function(){
    map.doubleClickZoom.disable();
    setTimeout(function(){
      map.doubleClickZoom.enable();
    }, 500);
    map.setZoom(map.getZoom() - stepSize);
  });
  jQuery('.zoom-to-default').click(function() {
    setDefaultView();
  });
});
