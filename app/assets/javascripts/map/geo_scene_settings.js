jQuery(function() {
  jQuery('#map').each(function() {

    var START_VIEW = [20, 20];
    var START_ZOOM = 1;
    window.SUB_SCENES_CLUSTER_RADIUS = 50;
    window.INFO_CLUSTER_RADIUS = 50;
    window.SINGLE_MARKER_MODE = true;

    window.subSceneId = null;
    window.sceneJson = {};
    window.sceneLayer = {};
    window.infoButtonLayer = {};
    window.systemEffectInfoButtonLayer = {};
    window.subSceneLayer = {};
    window.systemEffectChainGroupLayer = {};

    var worldBounds = [[-90, -180], [90, 180]];
    window.map = L.map('map', {
      minZoom: 3,
      maxBounds: worldBounds
    });

    var addMap = function(url) {
      baselayer = L.tileLayer(url, {attribution: '&copy; <a href="https://osm.org/copyright">OpenStreetMap</a> contributors'});
      map.addLayer(baselayer);
      map.setView(START_VIEW, START_ZOOM);
    };

    var boundaryLayer = L.tileLayer('http://korona.geog.uni-heidelberg.de/tiles/adminb/x={x}&y={y}&z={z}');
    var boundaryToggle = jQuery('.toggle-boundaries');
    map.on('zoomend', function() {
      if (map.getZoom() >= 4) {
        boundaryToggle.show();
      } else {
        boundaryToggle.hide();
      }
    });
    boundaryToggle.click(function() {
      boundaryToggle.toggleClass('active');
      if (boundaryToggle.hasClass('active')) {
        map.addLayer(boundaryLayer);
      } else {
        map.removeLayer(boundaryLayer);
      };
    });

    addMap(window.tileLayerUrl);

    // ADDRESS SEARCH
    jQuery('.zoom-container').append("<i class='icon-location-1 geosearch'></i>");
    jQuery('.geosearch').click(function() {
      jQuery('#geocoder-modal').modal('show');
    });
    var geocoder = L.Control.geocoder({
      collapsed: false,
      defaultMarkGeocode: false,
      position: 'bottomright',
      placeholder: 'Search address...'
    })
    .on('markgeocode', function(e) {
      jQuery('#geocoder-modal').modal('hide');
      var bbox = e.geocode.bbox;
      map.fitBounds(bbox);
    })
    .addTo(map);
    jQuery('.leaflet-control-geocoder').appendTo('.geocoder-container');

    // GLOBAL FUNCTIONS
    setDefaultView = function() {
      if (window.currentSubSceneId != null && window.infoButtonBounds != null) {
        map.fitBounds(window.infoButtonBounds, {
          padding: [100, 100]
        });
      } else {
        map.setView(START_VIEW, START_ZOOM, {
          pan: { animate: true , duration: 1 },
          zoom: { animate: true },
          animate: true
        });
      };
    };

  }); // end of map scope //
});
