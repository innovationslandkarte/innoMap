function defineClusterIcon(cluster) {
  var children = cluster.getAllChildMarkers(),
    n = children.length,
    strokeWidth = 0.5,
    outerRadius = n>299 ? 40 : n>99 ? 35 : n>9 ? 25 : n>4 ? 17 : 14,
    innerRadius = n>999 ? 22 : n>99 ? 17 : n>9 ? 12 : 8,
    iconDim = (outerRadius + strokeWidth) * 2, //...and divIcon dimensions (leaflet really want to know the size)
    data = d3.nest()
      .key(function(d) { return d.feature.properties['category']; })
      .entries(children, d3.map),
    html = bakeThePie({ data: data, outerRadius: outerRadius, innerRadius: innerRadius, pieLabel: n }),
    myIcon = new L.DivIcon({
      html: html,
      className: 'marker-cluster',
      iconSize: new L.Point(iconDim, iconDim)
    });
  return myIcon;
};

/*function that generates a svg markup for the pie chart*/
function bakeThePie(options) {
  var data = options.data,
      outerRadius = options.outerRadius?options.outerRadius:28,
      innerRadius = options.innerRadius?options.innerRadius:10,
      pieLabel = options.pieLabel?options.pieLabel:'';

  var strokeWidth = 0.5, // to assure that css stroke is not cut off
      pieClass = 'cluster-pie',
      pieLabelClass = 'cluster-pie-label';

  var origo = (outerRadius + strokeWidth),
      w = origo * 2,
      h = w,
      donut = d3.layout.pie(),
      arc = d3.svg.arc().innerRadius(innerRadius).outerRadius(outerRadius);

  //Create an svg element
  var svg = document.createElementNS(d3.ns.prefix.svg, 'svg');

  //Create the pie chart
  var vis = d3.select(svg)
    .data([data])
    .attr('class', pieClass)
    .attr('width', w)
    .attr('height', h);

    vis.append('ellipse')
      .attr('cx', w/2)
      .attr('cy', w/2)
      .attr('rx', w/2)
      .attr('ry', w/2)
      .attr('fill', 'white');

  var arcs = vis.selectAll('g.arc')
    .data(donut.value(function(d){return d.values.length;}))
    .enter().append('svg:g')
    .attr('class', 'arc')
    .attr('transform', 'translate(' + origo + ',' + origo + ')');

  arcs.append('svg:path')
    .attr('fill', function(d) { return d.data.values[0].feature.properties.color })
    .attr('d', arc);

  vis.append('text')
    .attr('x',origo)
    .attr('y',origo)
    .attr('class', pieLabelClass)
    .attr('text-anchor', 'middle')
    //.attr('dominant-baseline', 'central')
    /*IE doesn't seem to support dominant-baseline, but setting dy to .3em does the trick*/
    .attr('dy','.3em')
    .text(pieLabel);

  //Return the svg-markup rather than the actual element
  return (new window.XMLSerializer()).serializeToString(svg)
};
