// Hide and show animations
$(function(){
  hideLoadingAnimation = function() {
    $('.loading').hide();
  }

  showLoadingAnimation = function() {
    $('.loading').show();
  }
})
