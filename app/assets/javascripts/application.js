// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require leaflet
//= require leaflet.markercluster
//= require Control.Geocoder
//= require jquery
//= require jquery.minicolors
//= require jquery_ujs
//= require jquery.easing.1.3.min
//= require jquery.form
//= require jquery.validate.min
//= require jquery-ui
//= require jquery.dd
//= require bootstrap.min
//= require aos
//= require owl.carousel.min
//= require jquery.isotope.min
//= require imagesloaded.pkgd.min
//= require jquery.easytabs.min
//= require viewport-units-buggyfill
//= require template_scripts
//= require froala_editor.min.js
//= require plugins/image.min.js
//= require plugins/draggable.min.js
//= require plugins/code_view.min.js
//= require plugins/lists.min.js
//= require plugins/link.min.js
//= require plugins/align.min.js
//= require plugins/video.min.js
//= require plugins/table.min.js
//= require plugins/font_size.min.js
//= require responsiveslides.js
//= require d3v3

jQuery(function() {
  //----- IFRAME -----//
  var withinIFrame = window.self !== window.top;
  if (withinIFrame) { jQuery('.toggle-navbar, .navbar').hide() };

  //----- MAP RESIZE -----//
  var resizeMap = function() {
    var windowHeight = jQuery(window).height();
    var navbar = jQuery('.navbar');
    if (navbar.is(':visible')) {
      var navbarHeight = navbar.outerHeight();
    } else {
      var navbarHeight = 0;
    };
    jQuery('.map').height(windowHeight - navbarHeight);
    jQuery('.info-overlay').css('top', navbarHeight);
    jQuery('.info-overlay').css('max-height', windowHeight - navbarHeight);
  };

  jQuery(window).resize(function() { resizeMap(); }).resize();

  jQuery('.navbar-collapse').on('shown.bs.collapse', function() { resizeMap(); });
  jQuery('.navbar-collapse').on('hidden.bs.collapse', function() { resizeMap(); });

  jQuery('.toggle-navbar').click(function() {
    jQuery(this).find('i').toggleClass('icon-down-open-1').toggleClass('icon-up-open-1');
    jQuery('.navbar').toggle();
    resizeMap();
  });

  jQuery.FroalaEditor.DEFAULTS.key = 'llhigF-11kD-8ebc1A-16B-13pC-11wD-8tdcB-16A2E-11E-11==';
  jQuery.FroalaEditor.DefineIcon('insertHighlightedLink', {NAME: 'star'});
  jQuery.FroalaEditor.RegisterCommand('insertHighlightedLink', {
    title: 'Insert highlighted link',
    focus: true,
    undo: true,
    refreshAfterCallback: true,
    callback: function () {
      this.html.insert("<div class='highlighted-link'><a href='Insert_any_URL'>Highlighted link</a></div>");
    }
  });
  var toolbarButtons = ['undo', 'redo', '|', 'bold', 'italic', 'fontSize', '|', 'align', '|', 'formatUL', 'formatOL', '|', 'insertLink', 'insertHighlightedLink', '|', 'insertImage', 'insertVideo', 'insertTable', 'insertHR', '|', 'html'];
  jQuery('.flash-message').delay(8000).fadeOut(800);
  jQuery('#form-modal').on('shown.bs.modal', function() {
    jQuery('.froala').froalaEditor({
      imageUploadURL: '/attachment/upload.json',
      imageUploadMethod: 'POST',
      imageDefaultWidth: '100%',
      imageMaxSize: 1024 * 1024 * 3,
      heightMin: 200,
      toolbarButtons: toolbarButtons,
      toolbarButtonsMD: toolbarButtons,
      toolbarButtonsSM: toolbarButtons,
      toolbarButtonsXS: toolbarButtons,
    });
  });
  jQuery('#form-modal').on('hidden.bs.modal', function() {
    jQuery('.froala').froalaEditor('destroy');
  });

  jQuery(document).ajaxError(function(event, xhr, status, error){
    var errors = xhr.responseText;
    jQuery('.modal-body > .alert-danger').remove();
    jQuery('.modal-body').append("<div class='alert alert-danger'>" + errors + "</div>");
    if (typeof grecaptcha !== 'undefined') {
      grecaptcha.reset();
    }
  });

  jQuery(document).ajaxSuccess(function(event, xhr, status){
    if (status.type == 'POST') {
      jQuery('.modal').modal('hide');
    };
  });

  jQuery('.info-overlay')
    .on('mouseover', function() {
      map.scrollWheelZoom.disable();
    })
    .on('mouseout', function() {
      map.scrollWheelZoom.enable();
    });

  jQuery('.info-overlay')
    .on('click', 'a:not([href="#"])', function() {
      window.open(this.href, 'newwindow', 'width=800, height=500');
      return false;
    });

  jQuery('#contact-map').each(function() {
    var map = L.map('contact-map', {
      scrollWheelZoom: false,
      touchZoom: false,
    });
    baselayer = L.tileLayer('http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png');
    map.addLayer(baselayer);
    var innozCoordinates = [52.481991, 13.357194];
    map.setView(innozCoordinates, 12);
    var icon =  L.icon({ iconUrl: window.markerIcon, iconSize: [25, 40], iconAnchor: [12, 40], popupAnchor: [0, -32] });
    var marker = L.marker(innozCoordinates, {icon: icon}).addTo(map).bindPopup("<a href='http://www.innoz.de'>Innovationszentrum für Mobilität und gesellschaftlichen Wandel GmbH</a>");
  });

  initRSlides = function() {
    var slideshowContainers = jQuery('canvas');
    jQuery(slideshowContainers).each(function(id, c) {
      var container = jQuery(c);
      var images = container.find('img').removeAttr('class');
      var newContainer = jQuery('<ul class="rslides"></ul>');
      jQuery(images).each(function(id, img) {
        var li = jQuery('<li></li>').append(img);
        newContainer.append(li);
      });
      container.replaceWith(newContainer);
    });

    jQuery('.rslides').responsiveSlides({
      auto: false,             // Boolean: Animate automatically, true or false
      speed: 500,             // Integer: Speed of the transition, in milliseconds
      timeout: 4000,          // Integer: Time between slide transitions, in milliseconds
      pager: true,           // Boolean: Show pager, true or false
      nav: false,             // Boolean: Show navigation, true or false
      random: false,          // Boolean: Randomize the order of the slides, true or false
      pause: false,           // Boolean: Pause on hover, true or false
      pauseControls: true,    // Boolean: Pause when hovering controls, true or false
      prevText: "<<<",   // String: Text for the "previous" button
      nextText: ">>>",       // String: Text for the "next" button
      maxwidth: "",           // Integer: Max-width of the slideshow, in pixels
      navContainer: "",       // Selector: Where controls should be appended to, default is after the 'ul'
      manualControls: "",     // Selector: Declare custom pager navigation
      namespace: "rslides",   // String: Change the default namespace used
      before: function(){},   // Function: Before callback
      after: function(){}     // Function: After callback
    });
  };
  initRSlides();

  var initImageDropdowns = function() {
    jQuery('body select.image-dropown').msDropDown();
    jQuery('.ddArrow').addClass('icon-down-dir-1');
  };

  jQuery('#form-modal').on('shown.bs.modal', function() {
    initImageDropdowns();
  });

  // Info button form
  jQuery(document).on('click', '.attribute-info-button', function() {
    jQuery(this).find('.attribute-info-text').show();
  });

  // user permissions form
  jQuery(document).on('change', '.read-permission-checkbox', function() {
    if (jQuery(this).prop('checked') == false) {
      jQuery(this).siblings().prop('checked', false);
    };
  });
  jQuery(document).on('change', '.write-permission-checkbox', function() {
    if (jQuery(this).prop('checked') == true) {
      jQuery(this).siblings('.read-permission-checkbox').prop('checked', true);
    } else {
      jQuery(this).siblings('.owner-checkbox').prop('checked', false);
    };
  });
  jQuery(document).on('change', '.owner-checkbox', function() {
    if (jQuery(this).prop('checked') == true) {
      jQuery(this).siblings().prop('checked', true);
    };
  });
});
