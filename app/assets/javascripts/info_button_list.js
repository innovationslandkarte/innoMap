jQuery(function() {
  if (window.userHasRights) {
    var showEditButtons = 'all';
  } else {
    var showEditButtons = 'never';
  };

  var columns = [{
    'data': 'name',
    className: 'all'
  }, {
    'data': 'category',
    className: 'all'
  }, {
    'data': 'created_at',
    className: 'none'
  }, {
    'data': 'edit',
    className: showEditButtons
  }, {
    'data': 'print',
    className: 'all'
  }, {
    'data': 'visible',
    className: 'none'
  }, ];

  for (var i = 0; i < window.numberOfAttributes; i++) {
    columns.push({
      className: 'none'
    })
  }

  jQuery('.content-table').each(function(index, div) {
    var table = jQuery(div).DataTable({
      'responsive': true,
      'searching': true,
      'dom': 't',
      'paging': false,
      'bInfo': false,
      'columns': columns,
      'order': [
        [1, 'asc'],
        [0, 'asc'],
      ]
    });

    // Attribute search fields
    table.columns().every(function() {
      var that = this;
      var title = this.header().textContent.trim();
      jQuery('[forAttribute="' + title + '"]').on('keyup change', function() {
        if (that.search() !== this.value) {
          that.search(this.value).draw();
        }
      });
    });
  });

  // PDF export
  jQuery('.select-all-for-pdf').click(function() {
    jQuery('.select-for-pdf').prop('checked', true);
  });
  jQuery('.unselect-all-for-pdf').click(function() {
    jQuery('.select-for-pdf').prop('checked', false);
  });
});