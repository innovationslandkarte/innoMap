module ApplicationHelper
  def on_map_with_writing_permission
    on_map && user_has_writing_permission
  end

  def on_map
    action_name == 'show_on_map'
  end

  def on_main_map
    on_map && controller_name == 'main_scenes'
  end

  def on_list
    action_name == 'info_button_list'
  end

  def on_main_scene_users
    @main_scene && controller_name == 'users'
  end

  def on_settings
    action_name == 'settings'
  end

  def user_has_writing_permission
    return false unless current_user
    return true if @main_scene && current_user.writing_permission_for?(@main_scene)
    return true if @geo_scene && current_user.writing_permission_for?(@geo_scene)
    return true if @scene && current_user.writing_permission_for?(@scene)
  end

  def public_contributions_allowed
    (@scene&.public_contributions_allowed) || (@geo_scene&.public_contributions_allowed)
  end
end
