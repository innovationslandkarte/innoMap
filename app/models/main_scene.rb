require 'sanitize'

class MainScene < InnoMapBaseModel
  mount_uploader :base_image, ImageUploader

  scope :public_access, -> { where(public: true) }

  has_many :scenes, dependent: :destroy
  has_many :graphical_scenes
  has_many :geo_scenes

  has_many :sub_scenes, through: :scenes
  has_many :info_buttons, through: :sub_scenes
  has_many :main_scene_categories, dependent: :destroy
  has_many :users, through: :main_scenes_users
  has_many :main_scenes_users

  nilify_blanks

  # Sanitization
  before_validation ->() { Sanitization.sanitize(resource: self, columns: %w[text welcome]) }

  validates :text, presence: true

  def base_image_ratio
    base_image.present? ? base_image.ratio : (1920.to_f / 1080.to_f)
  end

  def base_image_url_for_map
    base_image.present? ? base_image.url : ActionController::Base.helpers.asset_path('texture.jpg')
  end

  def json_with_content
    {
      id: id,
      name: text,
      scenes: scenes.all.map(&:geojson),
    }
  end

  def category_names
    if main_scene_categories.any?
      main_scene_categories.map(&:name).join(' | ')
    else
      'No categories'
    end
  end
end
