class CsvImporter
  require 'csv'

  def initialize(csv_path, sub_scene)
    @csv_path = csv_path
    @sub_scene = sub_scene
  end

  attr_accessor :sub_scene, :csv_path

  # rubocop:disable Metrics/AbcSize
  def import
    CSV.foreach(csv_path, headers: true) do |row|
      sub_scene.info_buttons.create(
        text: row['text'],
        latitude: row['latitude'].to_f,
        longitude: row['longitude'].to_f,
        # rubocop:disable Security/Eval
        content_store: eval(row['content_store']),
        icon: row['icon'],
        visible: row['visible'],
        long_title: row['long_title'],
        scene_category_id: row['scene_category_id']
      )
    end
  end
end
