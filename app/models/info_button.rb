require 'sanitize'

class InfoButton < InnoMapBaseModel
  # auditing
  has_paper_trail

  belongs_to :sub_scene
  belongs_to :scene_category

  # Sanitization
  before_validation ->() { Sanitization.sanitize(resource: self, columns: %w[content_store]) }

  mount_uploader :icon, ImageUploader

  validates :text, length: { minimum: 1 }
  validates :text, length: { maximum: 40 }
  validates :scene_category_id, numericality: { only_integer: true }
  validates :latitude, numericality: true
  validates :longitude, numericality: true

  default_scope { order(scene_category_id: :asc) }
  default_scope { order(text: :asc) }

  store :content_store, coder: JSON

  after_initialize :add_accessors_for_content_attributes

  def add_accessors_for_content_attributes
    content_attributes.each do |attr|
      singleton_class.class_eval do
        define_method(attr.accessor_name + '=') do |value|
          super(value)
        end
        store_accessor :content_store, attr.accessor_name
      end
    end
  end

  def content_attributes
    sub_scene&.scene&.content_attributes ||
      # mainly for testing purposes (factory_girl)
      (GeoScene.all_content_attributes + Scene.all_content_attributes).uniq
  end

  def full_content(attributes = nil)
    count = content_attributes.count
    html = ''
    content_attributes.each do |attr|
      show_attribute = attributes.nil? || attributes.include?(attr.name)
      next unless show_attribute && send(attr.accessor_name).present?
      if count > 1
        html << "<div class='content-attribute-name'>#{attr.cleaned_name}</div>"
      end
      html << "<div class='content-html-attribute'>#{send(attr.accessor_name).strip}</div>"
    end
    html
  end

  def color
    scene_category.color
  end

  def visible_or_recently_added
    visible || recently_added?
  end

  def recently_added?
    (Time.now - created_at) < 300
  end

  def longest_title
    long_title.present? ? long_title : text
  end

  # rubocop:disable Metrics/AbcSize, Layout/MethodLength
  def geojson
    {
      id: id,
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [longitude, latitude],
      },
      properties: {
        title: text,
        longest_title: longest_title,
        content: full_content,
        sub_scene: sub_scene_id,
        category: scene_category_id,
        icon: (ActionController::Base.helpers.image_tag(icon.url, alt: nil) if icon.url),
        icon_size: icon_size,
        color: color || 'grey',
        system_effect_id: system_effect_id,
        recent_guest_contribution: recently_added? && !visible,
        created_at: created_at,
      },
    }
  end
end
