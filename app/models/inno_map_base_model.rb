class InnoMapBaseModel < ActiveRecord::Base
  require 'csv'

  self.abstract_class = true

  def self.to_csv(filename)
    CSV.open(filename, 'w') do |csv|
      csv << attribute_names
      all.each do |record|
        csv << record.attributes.values
      end
    end
  end
end
