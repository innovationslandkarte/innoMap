class Versions
  def initialize(versions)
    @versions = versions
  end

  attr_accessor :versions

  def readable
    @versions.map do |version|
      next unless version.changeset.present?
      string_from_changes(version)
    end
  end

  def string_from_changes(version)
    string = ''
    version.changeset.each do |key, change_array|
      string << key + ': '
      string << change_array[0].to_s + ' >>> ' + change_array[1].to_s
    end
    string
  end
end
