class SubScene < InnoMapBaseModel
  belongs_to :scene
  has_many :info_buttons, dependent: :destroy

  # Sanitization
  before_validation ->() { Sanitization.sanitize(resource: self, columns: %w[text]) }

  validates :text, length: { minimum: 1 }
  validates :latitude, numericality: true
  validates :longitude, numericality: true

  mount_uploader :icon, ImageUploader

  def info_buttons_for_map
    info_buttons.where(system_effect_id: nil).all.find_all(&:visible_or_recently_added).map(&:geojson)
  end

  def geojson
    {
      id: id,
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [longitude, latitude],
      },
      properties: {
        icon: (ActionController::Base.helpers.image_tag(icon.url) if icon.url),
        icon_size: icon_size,
        text: text,
        bounds: bounds,
      },
    }
  end

  def bounds
    latitudes = info_buttons.map(&:latitude)
    longitudes = info_buttons.map(&:longitude)
    return false if latitudes.empty? || longitudes.empty?
    [
      [latitudes.min, longitudes.min],
      [latitudes.max, longitudes.max],
    ]
  end
end
