class ContentAttribute < ActiveRecord::Base
  belongs_to :scene

  default_scope { order(position: :asc) }

  before_save :default_position
  def default_position
    current_max = scene.content_attributes.map(&:position).max
    incremented = (current_max&.+1).to_i
    self.position ||= [incremented, 1].max
  end

  def self.make_accessor_name(string)
    string.gsub(/[^0-9A-Za-z]/, '')
  end

  def accessor_name
    self.class.make_accessor_name(name)
  end

  def cleaned_name
    name.gsub('HTML', '').gsub('_', ' ').strip
  end

  after_update do
    scene.info_buttons.each do |info_button|
      content = info_button.content_store
      content[accessor_name] = content[self.class.make_accessor_name(name_was)]
      content.delete(name_was)
      info_button.update(content_store: content)
    end
  end

  after_destroy do
    scene.info_buttons.each do |info_button|
      content = info_button.content_store
      content.delete(accessor_name)
      info_button.update(content_store: content)
    end
  end
end
