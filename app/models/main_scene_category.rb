class MainSceneCategory < InnoMapBaseModel
  belongs_to :main_scene

  mount_uploader :icon, ImageUploader
  mount_uploader :image, ImageUploader

  default_scope { order(position: :asc) }

  validates :name, length: { minimum: 1 }

  validate :validate_color

  def validate_color
    return false if color =~ /^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/i
    errors.add(:color, 'muss ein gültiger HEX-Wert sein.')
  end
end
