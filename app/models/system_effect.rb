class SystemEffect < InnoMapBaseModel
  belongs_to :system_effect_chain

  validates :text, length: { minimum: 1 }
  validates :latitude, numericality: true
  validates :longitude, numericality: true
  validates :position, numericality: { only_integer: true }

  default_scope { order(position: :asc) }

  def info_buttons
    InfoButton.where(system_effect_id: id)
  end

  # rubocop:disable Metrics/MethodLength
  def geojson
    {
      id: id,
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [longitude, latitude],
      },
      properties: {
        text: text,
        content: content,
        systemEffectChain: system_effect_chain_id,
        position: position,
        longitude: longitude,
        latitude: latitude,
      },
      infoButtons: info_buttons.where(visible: true).map(&:geojson),
    }
  end
end
