class SceneCategory < InnoMapBaseModel
  belongs_to :scene

  has_many :info_buttons, dependent: :destroy
  has_many :system_effect_chains, dependent: :destroy
  has_many :system_effects, through: :system_effect_chains, dependent: :destroy

  mount_uploader :icon, ImageUploader
  mount_uploader :image, ImageUploader
  mount_uploader :image_overlay, ImageUploader

  validate :validate_color
  validates :name, length: { minimum: 1 }

  default_scope { order(position: :asc) }

  def validate_color
    return false if color =~ /^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/i
    errors.add(:color, 'muss ein gültiger HEX-Wert sein.')
  end
end
