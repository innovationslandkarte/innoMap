require 'sanitize'

class Scene < InnoMapBaseModel

  def self.all_content_attributes
    all.map(&:content_attributes).flatten.compact
  end

  scope :graphical_scenes, -> { where(type: 'GraphicalScene') }
  scope :geo_scenes, -> { where(type: 'GeoScene') }

  belongs_to :main_scene

  has_many :sub_scenes, dependent: :destroy
  has_many :content_attributes, dependent: :destroy
  has_many :scene_categories, dependent: :destroy
  has_many :categories, through: :scene_categories
  has_many :info_buttons, through: :sub_scenes
  has_many :system_effect_chains, through: :scene_categories

  validates :text, length: { minimum: 1 }
  validates :latitude, numericality: true
  validates :longitude, numericality: true

  accepts_nested_attributes_for :content_attributes, reject_if: :all_blank, allow_destroy: true

  mount_uploader :icon, ImageUploader
  mount_uploader :base_image, ImageUploader
  mount_uploader :base_image_overlay, ImageUploader

  # Sanitization
  before_validation ->() { Sanitization.sanitize(resource: self, columns: %w[text welcome]) }

  def info_buttons_for_map
    info_buttons.where(system_effect_id: nil).all.find_all(&:visible_or_recently_added).map(&:geojson)
  end

  def json_with_content
    {
      id: id,
      name: text,
      mainSceneId: main_scene_id,
      subScenes: sub_scenes.map(&:geojson),
      infoButtons: info_buttons_for_map,
      systemEffectChains: system_effect_chains.map(&:geojson),
    }
  end

  def geojson
    {
      id: id,
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [longitude, latitude],
      },
      properties: {
        text: text,
        icon: (ActionController::Base.helpers.image_tag(icon.url) if icon.url),
        icon_size: icon_size,
        type: type,
      },
    }
  end

  def category_names
    if scene_categories.any?
      scene_categories.map(&:name).join(' | ')
    else
      'No categories'
    end
  end
end
