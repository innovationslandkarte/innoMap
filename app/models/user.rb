class User < InnoMapBaseModel
  has_secure_password
  validates :password, presence: true, confirmation: true, length: { minimum: 8 }, allow_nil: true

  has_many :main_scenes_users
  has_many :main_scenes, through: :main_scenes_users
  accepts_nested_attributes_for :main_scenes_users, reject_if: :all_blank, allow_destroy: true

  scope :admins, -> { where(admin: true) }

  def assign_permission_for(main_scene, edit: false, own: false)
    MainScenesUser.create(user_id: id, main_scene_id: main_scene.id, edit: edit, own: own)
  end

  def main_scenes_as_owner
    return main_scenes if admin?
    main_scenes_users.where(own: true).map(&:main_scene)
  end

  def main_scenes_as_editor
    return main_scenes if admin?
    main_scenes_users.where(edit: true).map(&:main_scene)
  end

  def owner_of?(scene)
    return true if admin?
    main_scene_id = scene.class.name == 'MainScene' ? scene.id : scene.main_scene.id
    main_scenes_users.find_by(main_scene_id: main_scene_id)&.own?
  end


  def writing_permission_for?(scene)
    return true if admin?
    main_scene_id = scene.class.name == 'MainScene' ? scene.id : scene.main_scene.id
    main_scenes_users.find_by(main_scene_id: main_scene_id)&.edit?
  end

  def reading_permission_for?(scene)
    return true if scene.public? || admin?
    main_scene = scene.class.name == 'MainScene' ? scene : scene.main_scene
    main_scenes.include?(main_scene)
  end

  def has_permission_to_destroy_user?(user)
    return true if admin?
    user.main_scenes.each do |ms|
      return false unless main_scenes.include?(ms)
    end
    return true
  end
end
