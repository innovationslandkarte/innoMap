class GeoScenesUser < InnoMapBaseModel
  belongs_to :user
  belongs_to :geo_scene
end
