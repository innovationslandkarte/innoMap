class GeoScene < Scene
  def tile_layer_url
    tile_layers[tile_layer][:url]
  end

  def tile_layers
    hash = {}
    tile_layer_info.each do |k, v|
      hash[k] = v.merge(sample: url_sample_tile(v[:url]))
    end
    hash
  end

  def tile_layer_sample
    url_sample_tile(tile_layers[tile_layer][:sample])
  end

  # rubocop:disable Metrics/MethodLength
  def tile_layer_info
    {
      1 => {
        url: 'http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png',
        name: 'SimplifiedWhite',
      },
      2 => {
        url: 'http://tile.openstreetmap.org/{z}/{x}/{y}.png',
        name: 'Openstreetmap',
      },
      3 => {
        url: 'http://{s}.tile.stamen.com/watercolor/{z}/{x}/{y}.png',
        name: 'Watercolor',
      },
      4 => {
        url: 'https://services.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}.jpg',
        name: 'Topographic',
      },
      5 => {
        url: 'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}.jpg',
        name: 'Satellite',
      },
      6 => {
        url: 'https://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png',
        name: 'Openstreetmap (hot)',
      }
    }
  end

  def url_sample_tile(url)
    url
      .gsub('{s}', 'a')
      .gsub('{z}', '11')
      .gsub('{x}', '596')
      .gsub('{y}', '776')
  end
end
