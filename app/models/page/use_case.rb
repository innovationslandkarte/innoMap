# rubocop:disable Metrics/LineLength
class Page::UseCase
  include Singleton

  def self.all
    subclasses.map(&:instance).sort_by(&:order)
  end

  class Messen < Page::UseCase
    def order
      1
    end

    def name
      'Messen und Events'
    end

    def description
      'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et'
    end
  end

  class Schmu < Page::UseCase
    def order
      1
    end

    def name
      'Schmu und Schmarn'
    end

    def description
      'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et'
    end
  end

  class Kartoffel < Page::UseCase
    def order
      1
    end

    def name
      'Kartoffelsalat'
    end

    def description
      'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et'
    end
  end
end
