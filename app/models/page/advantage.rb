# rubocop:disable Metrics/LineLength
class Page::Advantage
  include Singleton

  def self.all
    subclasses.map(&:instance).sort_by(&:order)
  end

  class Strategy < Page::Advantage
    def name
      if I18n.locale == :de
        'Strategie'
      else
        'Strategy'
      end
    end

    def order
      1
    end

    def icon_css_class
      'icon-flow-tree'
    end

    def description
      if I18n.locale == :de
        'Die Karte kann für strategische Zwecke eingesetzt werden wie Teambuilding oder als Gesamtübersicht bei der Projektplanung.'
      else
        'This map can be used for strategic matters, team building or create overviews for project management.'
      end
    end
  end

  class Communication < Page::Advantage
    def name
      if I18n.locale == :de
        'Kommunikation'
      else
        'Communication'
      end
    end

    def order
      2
    end

    def icon_css_class
      'icon-users'
    end

    def description
      if I18n.locale == :de
        'Unternehmensvernetzung nimmt im digitalen Zeitalter einen immer höheren Stellenwert ein. Die Innovationslandkarte lässt sich dabei hervorragend als internes Kommunikationstool für Ihre Mitarbeiter nutzen.'
      else
        'Unternehmensvernetzung nimmt im digitalen Zeitalter einen immer höheren Stellenwert ein. Die Innovationslandkarte lässt sich dabei hervorragend als internes Kommunikationstool für Ihre Mitarbeiter nutzen.'
      end
    end
  end

  class Marketing < Page::Advantage
    def name
      if I18n.locale == :de
        'Marketing'
      else
        'Marketing'
      end
    end

    def order
      3
    end

    def icon_css_class
      'icon-map'
    end

    def description
      if I18n.locale == :de
        'Ob als „digitale Visitenkarte“ in der Lobby oder als Vertriebstool auf Messen und Events: Mit der Innovationslandkarte landen Sie bei ihrer Zielgruppe stets einen Volltreffer.'
      else
        'Ob als „digitale Visitenkarte“ in der Lobby oder als Vertriebstool auf Messen und Events: Mit der Innovationslandkarte landen Sie bei ihrer Zielgruppe stets einen Volltreffer.'
      end
    end
  end

  class Collaboration < Page::Advantage
    def name
      if I18n.locale == :de
        'Zusammenarbeit'
      else
        'Zusammenarbeit'
      end
    end

    def order
      4
    end

    def icon_css_class
      'icon-network'
    end

    def description
      if I18n.locale == :de
        'Gruppenarbeit macht Spaß und fördert sowohl die Kreativität als auch die Produktivität Ihrer Mitarbeiter. Die Innovationslandkarte begünstigt die dynamische Zusammenarbeit von physischen und virtuellen Teams in Ihrem Unternehmen.'
      else
        'Gruppenarbeit macht Spaß und fördert sowohl die Kreativität als auch die Produktivität Ihrer Mitarbeiter. Die Innovationslandkarte begünstigt die dynamische Zusammenarbeit von physischen und virtuellen Teams in Ihrem Unternehmen.'
      end
    end
  end

  class Technology < Page::Advantage
    def name
      if I18n.locale == :de
        'Technologie'
      else
        'Technologie'
      end
    end

    def order
      4
    end

    def icon_css_class
      'icon-cog-alt'
    end

    def description
      if I18n.locale == :de
        'Die Karte ist multi-device fähig und kann in vielen verschiedenen Settings integriert werden um jedem Anspruch gerecht zu werden.'
      else
        'Die Karte ist multi-device fähig und kann in vielen verschiedenen Settings integriert werden um jedem Anspruch gerecht zu werden.'
      end
    end
  end

  class Design < Page::Advantage
    def name
      if I18n.locale == :de
        'Design'
      else
        'Design'
      end
    end

    def order
      4
    end

    def icon_css_class
      'icon-feather'
    end

    def description
      if I18n.locale == :de
        'Ebenso sind die Inhalte der Karte durch die räumliche Aufteilung wesentlich einprägsamer.'
      else
        'Ebenso sind die Inhalte der Karte durch die räumliche Aufteilung wesentlich einprägsamer.'
      end
    end
  end
end
