# rubocop:disable Metrics/LineLength
class Page::Feature
  include Singleton

  def self.all
    subclasses.map(&:instance).sort_by(&:order)
  end

  class Welcome < Page::Feature
    def name
      if I18n.locale == :de
        'Willkommen'
      else
        'Welcome'
      end
    end

    def order
      1
    end

    def icon_css_class
      'icon-newspaper'
    end

    def description
      if I18n.locale == :de
        'Der erste Eindruck zählt - Begrüßen Sie Nutzer mit einer Einleitung zum Gesamtprojekt.'
      else
        'A welcome screen briefs the viewers on the topic at hand.'
      end
    end
  end

  class Zoom < Page::Feature
    def name
      if I18n.locale == :de
        'Zoom'
      else
        'Zoom'
      end
    end

    def order
      2
    end

    def icon_css_class
      'icon-search-1'
    end

    def description
      if I18n.locale == :de
        'In einer Übersichtskarte werden einzelne, betretbare Regionen dargestellt. Jede Region ist nochmals in Quartiere einteilbar.'
      else
        'Clickable regions are displayed on an overview map. Every region can be divided into subregions.'
      end
    end
  end

  class Language < Page::Feature
    def name
      if I18n.locale == :de
        'Sprache'
      else
        'Language'
      end
    end

    def order
      3
    end

    def icon_css_class
      'icon-chat-1'
    end

    def description
      if I18n.locale == :de
        'Begeistere multilingual - Direktzugang zu verschiedenen Sprachen der Inhalte.'
      else
        'Direct access to  different languages.'
      end
    end
  end

  class Filter
    def name
      if I18n.locale == :de
        'Themenfilter'
      else
        'Topic filter'
      end
    end

    def order
      4
    end

    def icon_css_class
      'icon-list-bullet'
    end

    def description
      if I18n.locale == :de
        'Was dich interessiert - Frei konfigurierbare Filter sortieren Inhalte.'
      else
        'Let the map show you the topics that you are interested in.'
      end
    end
  end

  class Search < Page::Feature
    def name
      if I18n.locale == :de
        'Suche'
      else
        'Search'
      end
    end

    def order
      5
    end

    def icon_css_class
      'icon-search-1'
    end

    def description
      if I18n.locale == :de
        'Auf der Suche? - Finde Inhalte mit der Direktsuche'
      else
        'Looking for specific content?'
      end
    end
  end
end
