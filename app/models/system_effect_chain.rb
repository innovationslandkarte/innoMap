class SystemEffectChain < InnoMapBaseModel
  belongs_to :scene_category
  has_many :system_effects, dependent: :destroy

  validates :text, length: { minimum: 1 }
  validates :scene_category_id, presence: { message: 'muss gewählt werden.' }

  accepts_nested_attributes_for :system_effects, reject_if: :all_blank, allow_destroy: true

  def scene_category
    SceneCategory.find(scene_category_id)
  end

  def scene
    scene_category.scene
  end

  def color
    scene_category.color
  end

  def geojson
    {
      id: id,
      properties: {
        text: text,
        category: scene_category_id,
        color: color || 'grey',
      },
      systemEffects: system_effects.all.map(&:geojson),
    }
  end
end
