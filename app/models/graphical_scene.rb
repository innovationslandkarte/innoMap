class GraphicalScene < Scene
  validates :base_image, presence: true

  def base_image_ratio
    base_image.ratio
  end
end
