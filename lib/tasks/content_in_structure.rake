namespace :content_in_structure do
  desc 'bring content in new attributized structure'
  task run: :environment do
    sub_scene = SubScene.find_by(text: 'InnoZ-Projektlandkarte')
    sub_scene.info_buttons.all.each do |i|
      p '-----------------------------------'
      p i.text
      pairs = i.send('content').split('<strong>')
      clean_pairs = pairs.map { |pair| pair.split('</strong>') }

      clean_pairs.each do |pair|
        key = pair[0]&.gsub(':', '')&.gsub('&nbsp;', '')&.gsub('<br>', '')&.strip
        value = pair[1]&.gsub(':', '')&.gsub('&nbsp;', '')&.gsub('<br>', '')&.gsub('<p>', '')&.gsub('</p>', '')&.strip
        skip if key.nil? || value.nil?

        found_attr = i.content_attributes.find { |a| a.gsub('HTML', '').strip.downcase == key.downcase }
        if found_attr
          i.update_attribute(found_attr, value)
          p "#{key} updated"
        else
          p "#{key} not present"
        end
      end
    end
  end
end
