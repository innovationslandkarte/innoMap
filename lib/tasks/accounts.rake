require 'securerandom'

namespace :accounts do
  desc 'make accounts from list'
  task make_from_list: :environment do
    %w[
      test@test.com
      jo5cha@web.de
    ].each do |email|
      password = SecureRandom.hex(4)
      User.create(
        email: email,
        password: password,
        password_confirmation: password,
        admin: false
      ).save
      puts "Email: #{email}, Password: #{password}"
    end
  end

  desc 'make accounts from list'
  task make: :environment do
    ARGV.each { |a| task a.to_sym do ; end }

    password = SecureRandom.hex(4)
    User.create(
      email: ARGV[1],
      password: password,
      password_confirmation: password,
      admin: ARGV[2] || false
    ).save
    puts "Email: #{ARGV[1]}, Password: #{password}"
  end
end
