# rubocop:disable Metrics/BlockLength
namespace :clone do
  desc 'clone a geo scene with almost all features (PLEASE USE WITH CARE!)'
  task geo_scene: :environment do
    GEO_SCENE_ID = 1
    source_scene = GeoScene.find(GEO_SCENE_ID)


    scene = GeoScene.create(
      text: source_scene.text,
      welcome: source_scene.welcome,
      public: source_scene.public,
      public_contributions_allowed: source_scene.public_contributions_allowed,
      tile_layer: source_scene.tile_layer
    )

    source_scene.content_attributes.each do |source_attribute|
      scene.content_attributes.create(
        info: source_attribute.info,
        options: source_attribute.options,
        position: source_attribute.position,
        name: source_attribute.name
      )
    end

    source_scene.scene_categories.each do |source_category|
      scene.scene_categories.create(
        name: source_category.name,
        position: source_category.position,
        color: source_category.color,
        image: (open(source_category.image.path) if source_category.image.url),
        image_overlay: (open(source_category.image_overlay.path) if source_category.image_overlay.url),
        icon: (open(source_category.icon.path) if source_category.icon.url)
      )
    end

    source_scene.sub_scenes.each do |source_sub_scene|
      sub_scene = scene.sub_scenes.create(
        text: source_sub_scene.text,
        latitude: source_sub_scene.latitude,
        longitude: source_sub_scene.longitude,
        icon: (open(source_sub_scene.icon.path) if source_sub_scene.icon.url),
        icon_size: source_sub_scene.icon_size
      )

      source_sub_scene.info_buttons.each do |source_button|
        category_name = source_button.scene_category.name
        sub_scene.info_buttons.create(
          text: source_button.text,
          content: source_button.content,
          latitude: source_button.latitude,
          longitude: source_button.longitude,
          icon: (open(source_button.icon.path) if source_button.icon.url),
          scene_category_id: sub_scene.parent.scene_categories.find_by(name: category_name).id,
          visible: source_button.visible,
          long_title: source_button.long_title,
          icon_size: source_button.icon_size,
          content_store: source_button.content_store
        )
      end
    end
  end
end
