# This module sanitizes any given attribute for a given object based on a rule set defined in Sanitization::Rules
module Sanitization
  module Rules
    def self.tags
      # iframe is included to make video embedding possible.
      # this must be restricted to specific pages as youtube and video
      %w[p br hr strong em span ol ul li a div img video table tbody tr td iframe video]
    end

    def self.attributes
      %w[align color size href style class src data-status contenteditable draggable controls width height frameborder autoplay poster]
    end
  end

  def self.sanitize(resource:, columns:)
    columns.each do |column|
      unsanitized_html = resource.send(column)

      sanitized_html = if is_sort_of_hash?(unsanitized_html)
                         sanitize_hash(unsanitized_html)
                       else
                         sanitize_string(unsanitized_html)
                       end

      resource.send(column + '=', sanitized_html)
    end

    resource
  end

  def self.sanitize_hash(hash)
    # Assume non-nested hash here (-> catch nevertheless but do nothing in case value [v] is [accidentally] no hash)
    hash.each { |k, v| hash[k] = sanitize_string(v) }
  end

  def self.sanitize_string(string)
    Rails::Html::WhiteListSanitizer.new.sanitize(string, tags: Rules.tags, attributes: Rules.attributes)
  end

  def self.is_sort_of_hash?(obj)
    obj.class == (ActiveSupport::HashWithIndifferentAccess || Hash)
  end
end
