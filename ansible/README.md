# Ansible provisioning

Install a new system with:

    $ cd ansible
    $ ansible-playbook site.yml

You must run the command from the `ansible` directory so that it finds
the custom `ansible.cfg` with its settings.

## Setup

You should have received a vault password which is not checked in into
git. Put it on a single line into the git ignored file

    ansible/.vault_password.txt


## Prerequisites

Make sure you have a recent version of
[ansible](http://docs.ansible.com/ansible/) installed. Tested to work
with

    $ ansible --version
    ansible 2.2.0.0
