class AddPositionToSystemEffects < ActiveRecord::Migration
  def change
    SystemEffect.delete_all
    add_column :system_effects, :position, :integer, null: false
  end
end
