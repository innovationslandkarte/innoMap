class AddIconToInfoButtons < ActiveRecord::Migration
  def change
    add_column :info_buttons, :icon, :string
  end
end
