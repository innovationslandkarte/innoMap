class AddOptionsToContentAttributes < ActiveRecord::Migration
  def change
    add_column :content_attributes, :options, :string
  end
end
