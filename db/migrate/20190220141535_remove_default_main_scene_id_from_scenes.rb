class RemoveDefaultMainSceneIdFromScenes < ActiveRecord::Migration
  def change
    change_column_default :scenes, :main_scene_id, nil
  end
end
