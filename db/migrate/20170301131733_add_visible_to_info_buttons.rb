class AddVisibleToInfoButtons < ActiveRecord::Migration
  def change
    add_column :info_buttons, :visible, :boolean, null: false, default: true
  end
end
