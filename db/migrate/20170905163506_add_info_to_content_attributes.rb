class AddInfoToContentAttributes < ActiveRecord::Migration
  def change
    add_column :content_attributes, :info, :text
  end
end
