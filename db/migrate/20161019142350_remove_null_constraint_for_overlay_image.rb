class RemoveNullConstraintForOverlayImage < ActiveRecord::Migration
  def change
    change_column :overlays, :image, :string, null: true
  end
end
