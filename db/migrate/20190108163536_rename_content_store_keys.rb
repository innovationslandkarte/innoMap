class RenameContentStoreKeys < ActiveRecord::Migration
  def change
    InfoButton.all.each do |info_button|
      new_content_store = info_button.content_store.map { |key, value| [key.gsub(/[^0-9A-Za-z]/, ''), value] }.to_h
      info_button.update(content_store: new_content_store)
    end
  end
end
