class AddPublicToScenes < ActiveRecord::Migration
  def change
    add_column :scenes, :public, :boolean, null: false, default: true
  end
end
