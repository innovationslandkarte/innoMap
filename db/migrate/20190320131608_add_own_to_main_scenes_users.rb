class AddOwnToMainScenesUsers < ActiveRecord::Migration
  def change
    add_column :main_scenes_users, :own, :boolean, null: false, default: false
  end
end
