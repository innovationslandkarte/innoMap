class AddIconToScenesAndSubScenes < ActiveRecord::Migration
  def change
    add_column :scenes, :icon, :string
    add_column :sub_scenes, :icon, :string
  end
end
