class RenameLayerColumns < ActiveRecord::Migration
  def change
    rename_column :scenes, :base_layer_animation, :base_image_overlay
    rename_column :scenes, :base_layer, :base_image
  end
end
