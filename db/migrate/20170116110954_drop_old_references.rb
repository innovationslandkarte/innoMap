class DropOldReferences < ActiveRecord::Migration
  def change
    remove_column :scene_categories, :scene_id
    remove_column :sub_scenes, :scene_id
  end
end
