class ChangeReferenceOnInfoButtons < ActiveRecord::Migration
  def change
    remove_column :info_buttons, :category_id
    add_reference :info_buttons, :scene_category, index: true, null: false
  end
end
