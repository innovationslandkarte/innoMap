class CreateContentAttributes < ActiveRecord::Migration
  def change
    create_table :content_attributes do |t|
      t.references :attributable, polymorphic: true, index: {name: 'attributable_index'}
      t.text :name, null: false
      t.integer :position, null: false

      t.timestamps null: false
    end
  end
end
