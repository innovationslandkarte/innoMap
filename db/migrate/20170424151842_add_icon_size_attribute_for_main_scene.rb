class AddIconSizeAttributeForMainScene < ActiveRecord::Migration
  def change
    add_column :scenes, :icon_size, :integer, default: 80
  end
end
