class AddContentStoreToInfoButton < ActiveRecord::Migration
  def change
    add_column :info_buttons, :content_store, :text

    Scene.all.each do |scene|
      scene.content_attributes.create(name: 'HTML content')
    end
    GeoScene.all.each do |scene|
      scene.content_attributes.create(name: 'HTML content')
    end
    InfoButton.all.each do |info_button|
      old_content = info_button.content
      info_button.update(:'HTML content' => old_content)
    end
  end
end
