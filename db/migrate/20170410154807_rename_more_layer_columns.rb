class RenameMoreLayerColumns < ActiveRecord::Migration
  def change
    rename_column :main_scenes, :base_layer, :base_image
  end
end
