class CreateOverlays < ActiveRecord::Migration
  def change
    create_table :overlays do |t|
      t.string :image, null: false
      t.references :scene, index: true, foreign_key: true, null: false
      t.references :category, index: true, foreign_key: true, null: false

      t.timestamps null: false
    end
  end
end
