class CreateSystemEffects < ActiveRecord::Migration
  def change
    create_table :system_effects do |t|
      t.references :system_effect_chain, index: true, foreign_key: true
      t.string :text, null: false
      t.string :content
      t.float :latitude, null: false
      t.float :longitude, null: false

      t.timestamps null: false
    end
  end
end
