class CreateInfoButton < ActiveRecord::Migration
  def change
    create_table :info_buttons do |t|
      t.references :sub_scene, index: true, foreign_key: true
      t.string :text, null: false
      t.text :content
      t.integer :category, null: false
      t.float :latitude, null: false
      t.float :longitude, null: false

      t.timestamps null: false
    end
  end
end
