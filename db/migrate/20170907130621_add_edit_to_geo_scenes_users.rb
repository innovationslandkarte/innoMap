class AddEditToGeoScenesUsers < ActiveRecord::Migration
  def change
    add_column :geo_scenes_users, :edit, :boolean, default: false, null: false
    add_column :geo_scenes_users, :id, :primary_key
  end
end
