class CreateGeoScenes < ActiveRecord::Migration
  def change
    create_table :geo_scenes do |t|
      t.string :text, null: false

      t.timestamps null: false
    end
  end
end
