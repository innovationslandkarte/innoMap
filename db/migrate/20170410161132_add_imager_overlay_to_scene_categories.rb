class AddImagerOverlayToSceneCategories < ActiveRecord::Migration
  def change
    add_column :scene_categories, :image_overlay, :string
  end
end
