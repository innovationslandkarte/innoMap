class AddBaseLayerAnimationToMainScenes < ActiveRecord::Migration
  def change
    add_column :scenes, :base_layer_animation, :string
  end
end
