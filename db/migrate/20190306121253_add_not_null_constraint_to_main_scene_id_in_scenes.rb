class AddNotNullConstraintToMainSceneIdInScenes < ActiveRecord::Migration
  def change
    change_column :scenes, :main_scene_id, :string, null: false
  end
end
