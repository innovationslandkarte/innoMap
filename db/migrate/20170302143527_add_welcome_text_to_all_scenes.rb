class AddWelcomeTextToAllScenes < ActiveRecord::Migration
  def change
    add_column :scenes, :welcome, :text
    add_column :geo_scenes, :welcome, :text
    add_column :main_scenes, :welcome, :text
  end
end
