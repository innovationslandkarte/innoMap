class AddTileLayerToGeoScene < ActiveRecord::Migration
  def change
    add_column :geo_scenes, :tile_layer, :integer, null: false, default: 1
  end
end
