class AddPublicContributionsAllowedToScenesAndGeoScenes < ActiveRecord::Migration
  def change
    add_column :scenes, :public_contributions_allowed, :boolean, null: false, default: false
    add_column :geo_scenes, :public_contributions_allowed, :boolean, null: false, default: false
  end
end
