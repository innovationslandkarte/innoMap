class AddPositionToSceneCategoriesAndMainSceneCategories < ActiveRecord::Migration
  def change
    add_column :scene_categories, :position, :integer, null: false, default: 1
    add_column :main_scene_categories, :position, :integer, null: false, default: 1
  end
end
