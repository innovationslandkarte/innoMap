class AddTypeToScenes < ActiveRecord::Migration
  def change
    add_column :scenes, :type, :string, null: false, default: 'GraphicalScene'
  end
end
