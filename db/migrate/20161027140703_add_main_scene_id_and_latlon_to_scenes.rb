class AddMainSceneIdAndLatlonToScenes < ActiveRecord::Migration
  def change
    add_reference :scenes, :main_scene, index: true, null: false, default: 1
    add_column :scenes, :latitude, :float, default: rand(30..70)
    add_column :scenes, :longitude, :float, default: rand(30..70)
  end
end
