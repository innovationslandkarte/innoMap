class AllowNullForMainSceneIdInScenes < ActiveRecord::Migration
  def change
    change_column :scenes, :main_scene_id, :string, null: true
  end
end
