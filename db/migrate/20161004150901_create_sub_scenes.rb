class CreateSubScenes < ActiveRecord::Migration
  def change
    create_table :sub_scenes do |t|
      t.references :scene, index: true, foreign_key: true
      t.string :text, null: false
      t.float :latitude, null: false
      t.float :longitude, null: false

      t.timestamps null: false
    end
  end
end
