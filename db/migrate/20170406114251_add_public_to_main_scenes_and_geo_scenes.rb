class AddPublicToMainScenesAndGeoScenes < ActiveRecord::Migration
  def change
    add_column :main_scenes, :public, :boolean, null: false, default: true
    add_column :geo_scenes, :public, :boolean, null: false, default: true
  end
end
