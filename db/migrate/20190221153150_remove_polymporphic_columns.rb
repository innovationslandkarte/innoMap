class RemovePolymporphicColumns < ActiveRecord::Migration
  class OldGeoScene < ActiveRecord::Base
    self.table_name = 'geo_scenes'
  end

  def change
    add_column :sub_scenes, :scene_id, :integer, null: false, default: 0
    add_column :scene_categories, :scene_id, :integer, null: false, default: 0
    add_column :content_attributes, :scene_id, :integer, null: false, default: 0

    Scene.all.each do |scene|
      sub_scenes = SubScene.where(subscenable_type: 'Scene', subscenable_id: scene.id)
      sub_scenes.each do |sub_scene|
        sub_scene.update_attributes(scene_id: scene.id)
      end

      cats = SceneCategory.where(scenecategorizable_type: 'Scene', scenecategorizable_id: scene.id)
      cats.each do |cat|
        cat.update_attributes(scene_id: scene.id)
      end

      attributes = ContentAttribute.where(attributable_type: 'Scene', attributable_id: scene.id)
      attributes.each do |attribute|
        attribute.update_attributes(scene_id: scene.id)
      end
    end

    OldGeoScene.all.each do |old_geo_scene|
      main_scene = MainScene.create(text: old_geo_scene.text)
      geo_scene = main_scene.geo_scenes.create(
        text: old_geo_scene.text,
        tile_layer: old_geo_scene.tile_layer
      )

      sub_scenes = SubScene.where(subscenable_type: 'GeoScene', subscenable_id: old_geo_scene.id)
      sub_scenes.each do |sub_scene|
        sub_scene.update_attributes(scene_id: geo_scene.id)
      end

      cats = SceneCategory.where(scenecategorizable_type: 'GeoScene', scenecategorizable_id: old_geo_scene.id)
      cats.each do |cat|
        cat.update_attributes(scene_id: geo_scene.id)
      end

      attributes = ContentAttribute.where(attributable_type: 'GeoScene', attributable_id: old_geo_scene.id)
      attributes.each do |attribute|
        attribute.update_attributes(scene_id: geo_scene.id)
      end
    end

    remove_column :sub_scenes, :subscenable_type
    remove_column :sub_scenes, :subscenable_id

    remove_column :scene_categories, :scenecategorizable_type
    remove_column :scene_categories, :scenecategorizable_id

    remove_column :content_attributes, :attributable_type
    remove_column :content_attributes, :attributable_id

    drop_table :geo_scenes
  end
end
