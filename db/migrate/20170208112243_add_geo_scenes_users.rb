class AddGeoScenesUsers < ActiveRecord::Migration
  def change
    create_table :geo_scenes_users, id: false do |t|
      t.integer :user_id
      t.integer :geo_scene_id
    end
  end
end
