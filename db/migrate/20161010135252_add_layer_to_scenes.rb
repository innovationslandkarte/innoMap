class AddLayerToScenes < ActiveRecord::Migration
  def change
    add_column :scenes, :base_layer, :string
    add_column :scenes, :layer_1, :string
    add_column :scenes, :layer_2, :string
    add_column :scenes, :layer_3, :string
    add_column :scenes, :layer_4, :string
    add_column :scenes, :layer_5, :string
    add_column :scenes, :layer_6, :string
  end
end
