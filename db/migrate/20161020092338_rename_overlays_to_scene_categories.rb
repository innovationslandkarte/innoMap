class RenameOverlaysToSceneCategories < ActiveRecord::Migration
  def change
    rename_table :overlays, :scene_categories
  end
end
