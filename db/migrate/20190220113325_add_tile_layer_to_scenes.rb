class AddTileLayerToScenes < ActiveRecord::Migration
  def change
    add_column :scenes, :tile_layer, :integer, null: false, default: 1
  end
end
