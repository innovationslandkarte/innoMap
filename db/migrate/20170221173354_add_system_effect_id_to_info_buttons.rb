class AddSystemEffectIdToInfoButtons < ActiveRecord::Migration
  def change
    add_column :info_buttons, :system_effect_id, :integer
  end
end
