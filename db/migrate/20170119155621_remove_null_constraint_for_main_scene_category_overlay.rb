class RemoveNullConstraintForMainSceneCategoryOverlay < ActiveRecord::Migration
  def change
    change_column :main_scene_categories, :image, :string, null: true
  end
end
