class MakeSubScenesPolymorphic < ActiveRecord::Migration
  def change
    add_reference :sub_scenes, :subscenable, polymorphic: true, index: true

    SubScene.all.each do |s|
      s.update_attribute(:subscenable_id, s.scene_id)
      s.update_attribute(:subscenable_type, 'Scene')
    end
  end
end
