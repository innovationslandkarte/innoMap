class RemoveScenes < ActiveRecord::Migration
  def change
    add_column :scene_categories, :name, :text, null: true
    add_column :scene_categories, :color, :text, null: true
    add_column :scene_categories, :icon, :text, null: true

    add_column :main_scene_categories, :name, :text, null: true
    add_column :main_scene_categories, :color, :text, null: true
    add_column :main_scene_categories, :icon, :text, null: true

    (SceneCategory.all + MainSceneCategory.all).each do |sc|
      category = Category.find(sc.category_id)
      sc.update(
        name: category.name,
        icon: category.icon,
        color: category.color,
      )
    end

    change_column :scene_categories, :name, :text, null: false
    change_column :main_scene_categories, :name, :text, null: false

    remove_column :scene_categories, :category_id
    remove_column :main_scene_categories, :category_id
    drop_table :categories
  end
end
