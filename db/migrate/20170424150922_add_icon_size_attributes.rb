class AddIconSizeAttributes < ActiveRecord::Migration
  def change
    add_column :info_buttons, :icon_size, :integer, default: 80
    add_column :sub_scenes, :icon_size, :integer, default: 80
  end
end
