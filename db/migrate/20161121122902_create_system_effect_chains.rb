class CreateSystemEffectChains < ActiveRecord::Migration
  def change
    create_table :system_effect_chains do |t|
      t.references :scene_category, index: true, foreign_key: true
      t.string :text, null: false

      t.timestamps null: false
    end
  end
end
