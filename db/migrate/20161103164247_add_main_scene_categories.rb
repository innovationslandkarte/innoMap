class AddMainSceneCategories < ActiveRecord::Migration
  def change
    create_table :main_scene_categories do |t|
      t.string :image, null: false
      t.references :main_scene, index: true, foreign_key: true, null: false
      t.references :category, index: true, foreign_key: true, null: false

      t.timestamps null: false
    end
  end
end
