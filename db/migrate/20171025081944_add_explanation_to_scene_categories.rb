class AddExplanationToSceneCategories < ActiveRecord::Migration
  def change
    add_column :scene_categories, :explanation, :string
  end
end
