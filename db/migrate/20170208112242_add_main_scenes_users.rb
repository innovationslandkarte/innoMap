class AddMainScenesUsers < ActiveRecord::Migration
  def change
    create_table :main_scenes_users, id: false do |t|
      t.integer :user_id
      t.integer :main_scene_id
    end
  end
end
