class AddCategoryIdToInfoButtons < ActiveRecord::Migration
  def change
    add_reference :info_buttons, :category, index: true, null: false
    remove_column :info_buttons, :category
  end
end
