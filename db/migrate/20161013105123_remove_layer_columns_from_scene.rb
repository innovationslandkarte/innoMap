class RemoveLayerColumnsFromScene < ActiveRecord::Migration
  def change
    remove_column :scenes, :layer_1
    remove_column :scenes, :layer_2
    remove_column :scenes, :layer_3
    remove_column :scenes, :layer_4
    remove_column :scenes, :layer_5
    remove_column :scenes, :layer_6
  end
end
