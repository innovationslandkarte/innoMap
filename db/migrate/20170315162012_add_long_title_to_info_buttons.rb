class AddLongTitleToInfoButtons < ActiveRecord::Migration
  def change
    add_column :info_buttons, :long_title, :text
  end
end
