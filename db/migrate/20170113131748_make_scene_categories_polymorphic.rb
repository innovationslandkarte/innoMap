class MakeSceneCategoriesPolymorphic < ActiveRecord::Migration
  def change
    add_reference :scene_categories, :scenecategorizable, polymorphic: true

    add_index :scene_categories, [:scenecategorizable_id, :scenecategorizable_type], name: :scenecategorizable_index

    # SceneCategory.all.unscoped.each do |sc|
    #   sc.update_attribute(:scenecategorizable_id, sc.scene_id)
    #   sc.update_attribute(:scenecategorizable_type, 'Scene')
    # end
  end
end
