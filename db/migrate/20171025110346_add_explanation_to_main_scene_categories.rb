class AddExplanationToMainSceneCategories < ActiveRecord::Migration
  def change
    add_column :main_scene_categories, :explanation, :string
  end
end
