class RemoveContentFromInfoButtons < ActiveRecord::Migration
  def change
    remove_column :info_buttons, :content
  end
end
