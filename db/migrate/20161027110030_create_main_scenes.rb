class CreateMainScenes < ActiveRecord::Migration
  def change
    create_table :main_scenes do |t|
      t.string :text, null: false
      t.string :base_layer

      t.timestamps null: false
    end
  end
end
