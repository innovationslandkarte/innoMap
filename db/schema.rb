# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190320131608) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "attachments", force: :cascade do |t|
    t.string "image"
  end

  create_table "content_attributes", force: :cascade do |t|
    t.text     "name",                   null: false
    t.integer  "position",               null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.text     "info"
    t.string   "options"
    t.integer  "scene_id",   default: 0, null: false
  end

  create_table "info_buttons", force: :cascade do |t|
    t.integer  "sub_scene_id"
    t.string   "text",                             null: false
    t.float    "latitude",                         null: false
    t.float    "longitude",                        null: false
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.string   "icon"
    t.integer  "scene_category_id",                null: false
    t.integer  "system_effect_id"
    t.boolean  "visible",           default: true, null: false
    t.text     "long_title"
    t.integer  "icon_size",         default: 80
    t.text     "content_store"
  end

  add_index "info_buttons", ["scene_category_id"], name: "index_info_buttons_on_scene_category_id", using: :btree
  add_index "info_buttons", ["sub_scene_id"], name: "index_info_buttons_on_sub_scene_id", using: :btree

  create_table "main_scene_categories", force: :cascade do |t|
    t.string   "image"
    t.integer  "main_scene_id",             null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "position",      default: 1, null: false
    t.text     "name",                      null: false
    t.text     "color"
    t.text     "icon"
    t.string   "explanation"
  end

  add_index "main_scene_categories", ["main_scene_id"], name: "index_main_scene_categories_on_main_scene_id", using: :btree

  create_table "main_scenes", force: :cascade do |t|
    t.string   "text",                      null: false
    t.string   "base_image"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.text     "welcome"
    t.boolean  "public",     default: true, null: false
  end

  create_table "main_scenes_users", force: :cascade do |t|
    t.integer "user_id"
    t.integer "main_scene_id"
    t.boolean "edit",          default: false, null: false
    t.boolean "own",           default: false, null: false
  end

  create_table "overlays", force: :cascade do |t|
    t.string   "image",       null: false
    t.integer  "scene_id",    null: false
    t.integer  "category_id", null: false
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "overlays", ["category_id"], name: "index_overlays_on_category_id", using: :btree
  add_index "overlays", ["scene_id"], name: "index_overlays_on_scene_id", using: :btree

  create_table "scene_categories", force: :cascade do |t|
    t.string   "image"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "position",      default: 1, null: false
    t.string   "image_overlay"
    t.text     "name",                      null: false
    t.text     "color"
    t.text     "icon"
    t.string   "explanation"
    t.integer  "scene_id",      default: 0, null: false
  end

  create_table "scenes", force: :cascade do |t|
    t.string   "text",                                                    null: false
    t.datetime "created_at",                                              null: false
    t.datetime "updated_at",                                              null: false
    t.string   "base_image"
    t.string   "main_scene_id",                                           null: false
    t.float    "latitude",                     default: 34.0
    t.float    "longitude",                    default: 47.0
    t.text     "welcome"
    t.boolean  "public_contributions_allowed", default: false,            null: false
    t.string   "base_image_overlay"
    t.string   "icon"
    t.integer  "icon_size",                    default: 80
    t.string   "type",                         default: "GraphicalScene", null: false
    t.integer  "tile_layer",                   default: 1,                null: false
    t.boolean  "public",                       default: true,             null: false
  end

  add_index "scenes", ["main_scene_id"], name: "index_scenes_on_main_scene_id", using: :btree

  create_table "sub_scenes", force: :cascade do |t|
    t.string   "text",                    null: false
    t.float    "latitude",                null: false
    t.float    "longitude",               null: false
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "icon"
    t.integer  "icon_size",  default: 80
    t.integer  "scene_id",   default: 0,  null: false
  end

  create_table "system_effect_chains", force: :cascade do |t|
    t.integer  "scene_category_id"
    t.string   "text",              null: false
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "system_effect_chains", ["scene_category_id"], name: "index_system_effect_chains_on_scene_category_id", using: :btree

  create_table "system_effects", force: :cascade do |t|
    t.integer  "system_effect_chain_id"
    t.string   "text",                   null: false
    t.string   "content"
    t.float    "latitude",               null: false
    t.float    "longitude",              null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "position",               null: false
  end

  add_index "system_effects", ["system_effect_chain_id"], name: "index_system_effects_on_system_effect_chain_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email"
    t.string   "password_digest"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.boolean  "admin",           default: false, null: false
  end

  create_table "versions", force: :cascade do |t|
    t.string   "item_type",      null: false
    t.integer  "item_id",        null: false
    t.string   "event",          null: false
    t.string   "whodunnit"
    t.text     "object"
    t.datetime "created_at"
    t.text     "object_changes"
  end

  add_index "versions", ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id", using: :btree

  add_foreign_key "info_buttons", "sub_scenes"
  add_foreign_key "main_scene_categories", "main_scenes"
  add_foreign_key "system_effect_chains", "scene_categories"
  add_foreign_key "system_effects", "system_effect_chains"
end
