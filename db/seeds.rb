colors = ['#ed2121', '#E9ED21', '#464093', '#ED9621']

User.create(
  email: 'test@test.com',
  password: 'secretsecret',
  password_confirmation: 'secretsecret',
).save

User.create(
  email: 'admin@test.com',
  password: 'secretsecret',
  password_confirmation: 'secretsecret',
  admin: true,
).save

main_scene = MainScene.create(
  text: 'Main Scene Geo',
)

geo_scene = main_scene.geo_scenes.create(
  text: 'The World',
  welcome: Faker::Lorem.sentences(3).join(' '),
  latitude: 25,
  longitude: 35
)
geo_scene.content_attributes.create(
  name: 'content'
)
geo_scene.content_attributes.create(
  name: 'foo'
)
geo_scene.content_attributes.create(
  name: 'bar'
)

geo_category1 = geo_scene.scene_categories.create(
  name: 'Category 1',
  color: '#000000',
)
geo_category2 = geo_scene.scene_categories.create(
  name: 'Category 2',
  color: '#7c7c7c',
)

geo_subscene1 = geo_scene.sub_scenes.create(
  text: 'Germany',
  latitude: 52,
  longitude: 12,
)

(1..100).each do |number|
  geo_subscene1.info_buttons.new.update(
    text: Faker::Space.star,
    scene_category_id: geo_scene.scene_categories.order("RANDOM()").first.id,
    latitude: rand(0..180),
    longitude: rand(0..180),
  )
end

main_scene1 = MainScene.create(
  text: 'Main Scene A',
  base_image: open(Rails.root + 'app/assets/images/seed/world.jpeg'),
  welcome: Faker::Lorem.sentences(3).join(' '),
)

main_scene2 = MainScene.create(
  text: 'Main Scene B',
  base_image: open(Rails.root + 'app/assets/images/seed/world.jpeg'),
)

main_scene_category1 =  main_scene1.main_scene_categories.create(
  image: open(Rails.root + 'app/assets/images/seed/01.png')
)

scene1 = main_scene1.graphical_scenes.create(
  text: 'Scene 1',
  latitude: 60,
  longitude: 70,
  base_image: open(Rails.root + 'app/assets/images/seed/base.png'),
)

scene2 = main_scene1.graphical_scenes.create(
  text: 'Scene 2',
  latitude: 55,
  longitude: 75,
  base_image: open(Rails.root + 'app/assets/images/seed/base.png'),
  welcome: Faker::Lorem.sentences(3).join(' '),
)

category1 =  scene1.scene_categories.create(
  name: 'Cat 1',
  color: '#c0e9b1',
  image: open(Rails.root + 'app/assets/images/seed/01.png')
)

category2 = scene1.scene_categories.create(
  name: 'Cat 2',
  color: '#f0b5b5',
  image: open(Rails.root + 'app/assets/images/seed/02.png')
)

category3 = scene1.scene_categories.create(
  name: 'Cat 3',
  color: '#dbd8fb',
  image: open(Rails.root + 'app/assets/images/seed/03.png')
)

subscene1 = scene1.sub_scenes.create(
  text: 'Seed Subscene 1',
  latitude: 50,
  longitude: 50,
)

subscene2 = scene1.sub_scenes.create(
  text: 'Seed Subscene 2',
  latitude: 70,
  longitude: 20,
)

subscene3 = scene1.sub_scenes.create(
  text: 'Seed Subscene 3',
  latitude: 70,
  longitude: 50,
)

(1..100).each do |number|
  subscene1.info_buttons.new.update(
    text: Faker::Space.star,
    scene_category_id: scene1.scene_categories.order("RANDOM()").first.id,
    latitude: rand(0..100),
    longitude: rand(0..100),
  )
end

system_effect_chain1 = category1.system_effect_chains.create(
  text: 'System Effect'
)

@system_effect1 = system_effect_chain1.system_effects.create(
  text: 'First effect',
  longitude: 50,
  latitude: 50,
  position: 1
)

system_effect_chain1.system_effects.create(
  text: 'Second effect',
  longitude: 60,
  latitude: 50,
  position: 2
)

system_effect_chain1.system_effects.create(
  text: 'Third effect',
  longitude: 55,
  latitude: 52,
  position: 3
)
