# rubocop:disable Metrics/LineLength

def saoi
  screenshot_and_open_image
end

def log_in
  visit login_path
  find('.user-button').trigger('click')
  sleep(0.5)
  fill_in('Email', with: 'user@test.com')
  fill_in('Password', with: 'secret123')
  find('.btn', text: 'LOG IN').trigger('click')
  sleep(0.5)
end

def log_out
  find('.user-button').trigger('click')
end

def log_in_as_admin
  @admin = create(:user, admin: true, email: 'admin@test.com')
  find('.user-button').trigger('click')
  sleep(0.5)
  fill_in('Email', with: 'admin@test.com')
  fill_in('Password', with: 'secret123')
  find('.btn', text: 'LOG IN').trigger('click')
  sleep(0.5)
end

def create_stack(scene_type)
  if scene_type == :scene
    @main_scene = create(:main_scene)
    @scene = create(:scene, main_scene_id: @main_scene.id)
    @sub_scene = create(:sub_scene, scene_id: @scene.id)
    @scene_category = create(:scene_category, scene: @scene)
    @content_attribute = create(:content_attribute, scene: @scene)
  else
    @main_scene = create(:main_scene)
    @scene = create(:geo_scene, main_scene_id: @main_scene.id)
    @sub_scene = create(:sub_scene, scene_id: @scene.id)
    @scene_category = create(:scene_category, scene: @scene)
    @content_attribute = create(:content_attribute, scene: @scene)
  end
  create(:info_button, text: 'First Info Button', latitude: 60, longitude: 60, sub_scene_id: @sub_scene.id, scene_category_id: @scene_category.id)
  create(:info_button, text: 'Second Info Button', latitude: 55, longitude: 55, sub_scene_id: @sub_scene.id, scene_category_id: @scene_category.id)
end

def create_system_effect_chain
  @system_effect_chain = create(:system_effect_chain, scene_category_id: @scene_category.id)
  @system_effect = create(:system_effect, system_effect_chain_id: @system_effect_chain.id, position: 1)
  create(:system_effect, system_effect_chain_id: @system_effect_chain.id, position: 2, text: 'Second System Effect')
  create(:info_button, text: 'System Effect Info', sub_scene_id: @sub_scene.id, scene_category_id: @scene_category.id, system_effect_id: @system_effect.id)
  @system_effect_chain_2 = create(:system_effect_chain, scene_category_id: @scene_category.id)
  create(:system_effect, system_effect_chain_id: @system_effect_chain_2.id, position: 666, text: 'Another Second System Effect')
end

def start_session
  @user = create(:user)
  session[:user_id] = @user.id
end
