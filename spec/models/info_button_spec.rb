describe InfoButton do
  before do
    @scene = create(:geo_scene)
    @scene.content_attributes.create(name: 'foo')
    @scene.content_attributes.create(name: 'bar')
    @sub_scene = create(:sub_scene, scene_id: @scene.id)
    @scene_category = create(:scene_category, scene: @scene)

    @info_button = InfoButton.new(
      text: 'First Info Button',
      latitude: 60, longitude: 60,
      sub_scene_id: @sub_scene.id,
      scene_category_id: @scene_category.id
    )
  end

  it 'can have content_attributes as parent scene has' do
    @info_button.update(foo: 'text for foo', bar: 'text for bar')

    expect(@info_button.foo).to eq('text for foo')
  end

  it 'cannot have content_attributes as parent scene has not' do
    expect {
      @info_button.update(whatever: 'text for foo')
    }.to raise_error(ActiveRecord::UnknownAttributeError)
  end

  it 'keeps content if related content attribute name is changed' do
    @info_button.update(foo: 'text for foo', bar: 'text for bar')
    @scene.content_attributes.find_by(name: 'foo').update(name: 'changed foo')
    new_instance = InfoButton.find_by(text: @info_button.text)

    expect(new_instance.changedfoo).to eq('text for foo')
    expect { new_instance.foo }.to raise_error(NoMethodError)
    expect(new_instance.bar).to eq('text for bar')
  end

  it 'deletes content if related content attribute is deleted' do
    @info_button.update(foo: 'text for foo', bar: 'text for bar')
    @scene.content_attributes.find_by(name: 'foo').destroy
    new_instance = InfoButton.find_by(text: @info_button.text)

    expect(new_instance).not_to respond_to(:foo)
    expect(new_instance.content_store.key?('foo')).to be_falsy
    expect(new_instance.bar).to eq('text for bar')
  end

  context 'Callbacks' do
    context 'Sanitization' do
      it 'should properly sanitize content_store' do
        code = "<html><body><script type='text/javascript'>alert(\"test\")</script></body></html>"
        content_store = ActiveSupport::HashWithIndifferentAccess.new(content: code)
        scene = create(:info_button, content_store: content_store, sub_scene: @sub_scene, scene_category: @scene_category)

        expectation = {"content" => "alert(\"test\")" }
        expect(scene.content_store).to eq expectation
      end
    end
  end
end
