RSpec.describe Attachment, type: :model do
  before do
    @scene = create(:geo_scene)
    @scene.content_attributes.create(name: 'foo')
    @scene.content_attributes.create(name: 'bar')
    @sub_scene = create(:sub_scene, scene_id: @scene.id)
    @scene_category = create(:scene_category, scene: @scene)

    @info_button = InfoButton.new(
      text: 'First Info Button',
      latitude: 60, longitude: 60,
      sub_scene_id: @sub_scene.id,
      scene_category_id: @scene_category.id
    )
  end

  context 'Restrictions' do
    it 'cannot upload non-whitelisted content' do
    end
  end
end
