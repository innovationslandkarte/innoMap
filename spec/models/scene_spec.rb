describe Scene, type: :model do
  before do
    @main_scene = create(:main_scene)
    @scene = create(:scene, main_scene_id: @main_scene.id)
  end

  context 'content attributes' do
    it 'can be updated' do
      @scene.content_attributes.create(name: 'foo')
      @scene.content_attributes.create(name: 'bar')
      expect(@scene.content_attributes.map(&:name)).to contain_exactly('foo', 'bar')
    end

    it 'have incrementing positions' do
      @scene.content_attributes.create(name: 'first')
      @scene.content_attributes.create(name: 'second')
      @scene.content_attributes.create(name: 'thousand', position: 1000)
      expectation = ['first', 1], ['second', 2], ['thousand', 1000]
      expect(@scene.content_attributes.map { |a| [a.name, a.position] }).to contain_exactly(*expectation)
    end
  end
end
