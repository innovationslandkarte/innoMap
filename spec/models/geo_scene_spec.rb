describe GeoScene, type: :model do
  context 'content attributes' do
    before do
      @main_scene = create(:main_scene)
      @scene = create(:geo_scene)
    end

    it 'can be updated' do
      @scene.content_attributes.create(name: 'foo')
      @scene.content_attributes.create(name: 'bar')
      expect(@scene.content_attributes.map(&:name)).to contain_exactly('foo', 'bar')
    end

    it 'have incrementing positions' do
      @scene.content_attributes.create(name: 'first')
      @scene.content_attributes.create(name: 'second')
      @scene.content_attributes.create(name: 'thousand', position: 1000)
      expectation = ['first', 1], ['second', 2], ['thousand', 1000]
      expect(@scene.content_attributes.map { |a| [a.name, a.position] }).to contain_exactly(*expectation)
    end
  end

  context 'Callbacks' do
    context 'Sanitization' do
      it 'Should not allow javascript' do
        code = "<html><body><script type='text/javascript'>alert(\"test\")</script></body></html>"
        scene = create(:geo_scene, text: code)

        expect(scene.text).to eq 'alert("test")'
      end

      # COMPLEX STUFF
      it 'Can insert video' do
        video_code = "<p><span class=\"fr-video fr-dvb fr-draggable\" contenteditable=\"false\" draggable=\"true\"><video class=\"fr-draggable\" controls=\"\" src=\"https://i.froala.com/download/d8496f6bd14a5a06c973d08dec8c66189c2f01d4.mp4?1509011167\" style=\"width: 600px;\">Your browser does not support HTML5 video.</video></span></p>"
        scene = create(:geo_scene, text: video_code)

        expect(scene.text).to eq video_code
      end

      it 'Can insert image' do
        image_code = "<p><img src=\"https://imagejournal.org/wp-content/uploads/bb-plugin/cache/23466317216_b99485ba14_o-panorama.jpg\" style=\"width: 100%px;\" class=\"fr-fic fr-dib\"></p>"
        scene = create(:geo_scene, text: image_code)

        expect(scene.text).to eq image_code
      end

      it 'Can insert text of different size' do
        text_code = "<p><span style=\"font-size: 10px;\">qwdqwd</span></p>"
        scene = create(:geo_scene, text: text_code)

        expect(scene.text).to eq text_code
      end

      it 'Can align text' do
        alignment_text = "<p style=\"text-align: center;\">adqwf</p>"
        scene = create(:geo_scene, text: alignment_text)

        expect(scene.text).to eq alignment_text
      end

      it 'Can insert table' do
        table_code = "<table style=\"width: 100%;\"><tbody><tr><td style=\"width: 14.2857%;\"><br></td><td style=\"width: 14.2857%;\"><br></td><td style=\"width: 14.2857%;\"><br></td><td style=\"width: 14.2857%;\"><br></td><td style=\"width: 14.2857%;\"><br></td><td style=\"width: 14.2857%;\"><br></td><td style=\"width: 14.2857%;\"><br></td></tr><tr><td style=\"width: 14.2857%;\"><br></td><td style=\"width: 14.2857%;\"><br></td><td style=\"width: 14.2857%;\"><br></td><td style=\"width: 14.2857%;\"><br></td><td style=\"width: 14.2857%;\"><br></td><td style=\"width: 14.2857%;\"><br></td><td style=\"width: 14.2857%;\"><br></td></tr><tr><td style=\"width: 14.2857%;\"><br></td><td style=\"width: 14.2857%;\"><br></td><td style=\"width: 14.2857%;\"><br></td><td style=\"width: 14.2857%;\"><br></td><td style=\"width: 14.2857%;\"><br></td><td style=\"width: 14.2857%;\"><br></td><td style=\"width: 14.2857%;\"><br></td></tr></tbody></table>"
        table_code_san = "<table style=\"width: 100%;\"><tbody>\n<tr>\n<td style=\"width: 14.2857%;\"><br></td>\n<td style=\"width: 14.2857%;\"><br></td>\n<td style=\"width: 14.2857%;\"><br></td>\n<td style=\"width: 14.2857%;\"><br></td>\n<td style=\"width: 14.2857%;\"><br></td>\n<td style=\"width: 14.2857%;\"><br></td>\n<td style=\"width: 14.2857%;\"><br></td>\n</tr>\n<tr>\n<td style=\"width: 14.2857%;\"><br></td>\n<td style=\"width: 14.2857%;\"><br></td>\n<td style=\"width: 14.2857%;\"><br></td>\n<td style=\"width: 14.2857%;\"><br></td>\n<td style=\"width: 14.2857%;\"><br></td>\n<td style=\"width: 14.2857%;\"><br></td>\n<td style=\"width: 14.2857%;\"><br></td>\n</tr>\n<tr>\n<td style=\"width: 14.2857%;\"><br></td>\n<td style=\"width: 14.2857%;\"><br></td>\n<td style=\"width: 14.2857%;\"><br></td>\n<td style=\"width: 14.2857%;\"><br></td>\n<td style=\"width: 14.2857%;\"><br></td>\n<td style=\"width: 14.2857%;\"><br></td>\n<td style=\"width: 14.2857%;\"><br></td>\n</tr>\n</tbody></table>"
        scene = create(:geo_scene, text: table_code)

        expect(scene.text).to eq table_code_san
      end
    end
  end
end
