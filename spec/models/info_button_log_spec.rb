describe InfoButtonLog, type: :model do
  before do
    create_stack(:geo_scene)
    @info_button = @scene.sub_scenes.first.info_buttons.create(
      text: 'To be changed',
      latitude: 52,
      longitude: 12,
      scene_category_id: @scene_category.id,
      content_store: { first_attribute: 'foo', second_attribute: 'bar' }
    )
  end

  context 'makes content changes readable' do
    it 'can be updated' do
      @info_button.content_store['second_attribute'] = 'new'
      @info_button.text = 'Changed'
      @info_button.save
      changes_hash = InfoButtonLog.new(@info_button.versions).hash
      expect(changes_hash.first['name']).to eq('Changed')
      expect(changes_hash.first['changes']['text']).to eq([nil, 'To be changed'])
      expect(changes_hash.last['changes']['text']).to eq(['To be changed', 'Changed'])
      expect(changes_hash.last['changes']['second_attribute']).to eq(%w[bar new])
      expect(changes_hash.last['changes']['first_attribute']).to be_nil
    end
  end
end
