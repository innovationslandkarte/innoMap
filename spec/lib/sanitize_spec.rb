require 'sanitize'

describe Sanitization do
  context 'Hash' do
    it 'Can sanitize hash with multiple elements' do
      hash = {
        'Foo' => '<html><body>test</body></html>',
        'Bar' => '<html><body>test</body></html>',
        'Batz' => '<html><body>test</body></html>' }

      expectation = { 'Foo' => 'test', 'Bar' => 'test', 'Batz' => 'test' }
      expect(Sanitization.sanitize_hash(hash)).to eq expectation
    end
  end
end
