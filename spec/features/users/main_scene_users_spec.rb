feature 'Main Scene Users View', :js do
  before do
    @main_scene = create(:main_scene)
    @owner = create(:user).assign_permission_for(@main_scene, edit: true, own: true)

    @user_1 = create(:user, email: 'user1@email.com').assign_permission_for(@main_scene)
    @user_2 = create(:user, email: 'user2@email.com')

    visit root_path
    log_in
  end

  scenario 'shows users' do
    visit main_scene_users_path(main_scene_id: @main_scene.id)

    expect(page).to have_content('user1@email.com')
    expect(page).to_not have_content('user2@email.com')

    click_on 'Create new scene user'
    fill_in 'user_email', with: 'bla@blubb.com'
    fill_in 'user_password', with: '123456789'
    fill_in 'user_password_confirmation', with: '123456789'
    click_on 'Create'

    new_user = User.find_by(email: 'bla@blubb.com')
    expect(@main_scene.users).to include(new_user)

    main_scene_rights = new_user.main_scenes_users.find_by(main_scene_id: @main_scene.id)
    expect(main_scene_rights.edit).to be_falsy
    expect(main_scene_rights.own).to be_falsy
  end
end
