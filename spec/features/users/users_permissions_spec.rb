feature 'User Permissions', :js do
  before do
    @first_main_scene = create(:main_scene, text: 'First Main Scene', public: true)
    @first_geo_scene = create(:geo_scene, main_scene: @first_main_scene)
    @first_graphical_scene = create(:scene, main_scene: @first_main_scene)

    @second_main_scene = create(:main_scene, text: 'Second Main Scene', public: false)
    @second_geo_scene = create(:geo_scene, main_scene: @second_main_scene)
    @second_graphical_scene = create(:scene, main_scene: @second_main_scene)

    @third_main_scene = create(:main_scene, text: 'Third Main Scene', public: false)
    @fourth_main_scene = create(:main_scene, text: 'Fourth Main Scene', public: false)
    visit root_path
  end

  context 'on map overview' do
    scenario 'are respected in scene overview' do
      @user = create(:user)
      log_in
      @user.assign_permission_for(@second_main_scene, edit: true)
      @user.assign_permission_for(@third_main_scene)
      visit scene_overview_path
      sleep(0.5)
      ['First Main Scene', 'Second Main Scene', 'Third Main Scene'].each do |name|
        expect(page).to have_content(name)
      end
      expect(page).to_not have_content('Fourth Main Scene')
      expect(all('.icon-cog').count).to eq(1)
    end

    scenario 'admins have all rights' do
      log_in_as_admin
      visit scene_overview_path
      sleep(0.5)
      ['First Main Scene', 'Second Main Scene', 'Third Main Scene', 'Fourth Main Scene'].each do |name|
        expect(page).to have_content(name)
      end
      expect(all('.icon-cog').count).to eq(4)
    end
  end

  context 'on map view' do
    before do
      @user = create(:user)
      log_in
      @user.assign_permission_for(@second_main_scene, edit: true)
    end

    scenario 'user can edit scene only with writing permissions' do
      visit main_scene_settings_path(id: @second_main_scene.id)
      expect(page).to have_content('configuration')
      visit geo_scene_settings_path(id: @second_geo_scene.id)
      expect(page).to have_content('configuration')
      visit graphical_scene_settings_path(id: @second_graphical_scene.id)
      expect(page).to have_content('configuration')

      visit main_scene_settings_path(id: @first_main_scene.id)
      expect(page).to_not have_content('configuration')
      visit geo_scene_settings_path(id: @first_geo_scene.id)
      expect(page).to_not have_content('configuration')
      visit graphical_scene_settings_path(id: @first_graphical_scene.id)
      expect(page).to_not have_content('configuration')
    end

    scenario 'user see editor buttons only with writing permissions' do
      visit main_scene_map_path(id: @second_main_scene.id)
      expect(page).to have_css('.editor-button')
      visit geo_scene_map_path(id: @second_geo_scene.id)
      expect(page).to have_css('.editor-button')
      visit graphical_scene_map_path(id: @second_graphical_scene.id)
      expect(page).to have_css('.editor-button')

      visit main_scene_map_path(id: @first_main_scene.id)
      expect(page).to_not have_content('.editor-button')
      visit geo_scene_map_path(id: @first_geo_scene.id)
      expect(page).to_not have_css('.editor-button')
      visit graphical_scene_map_path(id: @first_graphical_scene.id)
      expect(page).to_not have_css('.editor-button')
    end
  end
end
