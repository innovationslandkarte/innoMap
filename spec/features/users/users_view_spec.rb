feature 'Users View', :js do
  before do
    @first_user = create(:user)
    @main_scene = create(:main_scene)

    visit root_path
  end

  scenario 'not accessible for non-admin users' do
    page.driver.headers = { 'ACCEPT-LANGUAGE' => 'de'  }
    log_in
    visit users_path

    expect(current_path).to eq('/de/login')
  end

  scenario 'shows users' do
    @second_user = create(:user, email: 'second_user@test.com')
    log_in_as_admin
    visit users_path
    expect(page).to have_content('admin@test.com')
    expect(page).to have_content('second_user@test.com')
  end

  scenario 'allows modification of user main scene rights' do
    log_in_as_admin
    visit users_path

    check_checkboxes(@main_scene, true, true)
    expect(@first_user.main_scenes.first).to eq(@main_scene)
    expect(@first_user.writing_permission_for?(@main_scene)).to be_truthy

    check_checkboxes(@main_scene, false, true)
    expect(@first_user.main_scenes.first).to eq(@main_scene)
    expect(@first_user.writing_permission_for?(@main_scene)).to be_truthy

    check_checkboxes(@main_scene, true, false)
    expect(@first_user.main_scenes.first).to eq(@main_scene)
    expect(@first_user.writing_permission_for?(@main_scene)).to be_falsy

    check_checkboxes(@main_scene, false, false)
    expect(@first_user.main_scenes.first).to eq(nil)
  end

  scenario 'allows modification of user password' do
    log_in_as_admin
    visit users_path

    expect { change_password }.to change { @first_user.reload.password_digest }
  end

  def change_password
    all('.icon-cog').first.trigger('click')
    sleep(0.5)
    fill_in('user_password',              with: 'newsecret123')
    fill_in('user_password_confirmation', with: 'newsecret123')

    click_on 'Save'
  end

  def check_checkboxes(scene, first, second)
    all('.icon-cog').first.trigger('click')
    sleep(0.5)
    checkboxes = if scene.class.name == 'MainScene'
                   find(".main_scene_#{scene.id}").find_all('input')
                 else
                   find(".geo_scene_#{scene.id}").find_all('input')
                 end
    checkboxes[0].set(first)
    checkboxes[1].set(second)
    click_on 'Save'
  end
end
