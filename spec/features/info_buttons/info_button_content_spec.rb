feature 'Info Button Content', :js do
  before do
    @user = create(:user)
    create_stack(:geo_scene)
    @user.assign_permission_for(@scene.main_scene, edit: true)
    log_in
    visit geo_scene_map_path(id: @scene.id, locale: :de)
  end

  scenario 'has working attribute text fields' do
    @scene.content_attributes.create(name: 'Foo (bar)')
    @scene.content_attributes.create(name: 'o O o?')
    @scene.content_attributes.create(name: 'donotshow')
    find('.add-button').trigger('click')
    page.find('#map').trigger('click')
    sleep(0.5)

    expect(page).to have_content('Foo (bar)')
    expect(page).to have_content('o O o?')
    expect(page).to have_content('donotshow')
    expect(page).to_not have_css('.fr-element')

    fill_in('Kurztitel (max. 40 Zeichen)', with: 'Info Button with content')
    fill_in('info_button_Foobar', with: 'Icecream for Foo') # brackets are missing in input id (see method 'accessor_name')
    fill_in('info_button_oOo', with: 'French fries for o O o?')
    click_on 'Speichern'
    find('.add-button').trigger('click')
    sleep(0.5)

    find('.zoom-out').click
    if find_all('.marker-cluster').any? # due to differing zoom level
      find_all('.marker-cluster')[1].trigger('click')
    end
    sleep(0.5)
    find('.map-button', text: 'Info Button with content').trigger('click')

    expect(page).to have_content('Foo (bar)')
    expect(page).to have_content('Icecream for Foo')
    expect(page).to have_content('o O o?')
    expect(page).to have_content('French fries for o O o?')
    expect(page).to_not have_content('donotshow')
  end

  scenario 'keeps content if attribute name is changed' do
    @scene.content_attributes.create(name: 'Foo (bar)')
    find('.add-button').trigger('click')
    page.find('#map').trigger('click')
    sleep(0.5)

    expect(page).to have_content('Foo (bar)')

    fill_in('Kurztitel (max. 40 Zeichen)', with: 'Info Button with content')
    fill_in('info_button_Foobar', with: 'Icecream for Foo') # brackets are missing in input id (see method 'accessor_name')
    click_on 'Speichern'
    sleep(0.5)

    ContentAttribute.find_by(name: 'Foo (bar)').update_attributes(name: 'New foo')
    visit current_path

    find('.zoom-out').click
    if find_all('.marker-cluster').any? # due to differing zoom level
      find_all('.marker-cluster')[1].trigger('click')
    end

    sleep(0.5)
    find('.map-button', text: 'Info Button with content').trigger('click')

    expect(page).to have_content('New foo')
    expect(page).to have_content('Icecream for Foo')
  end

  scenario 'has froala html editor when content attribute name contains HTML' do
    @scene.content_attributes.create(name: 'HTML Content')
    find('.add-button').trigger('click')
    page.find('#map').trigger('click')
    sleep(1)
    expect(page).to have_content('Content')
    expect(page).to have_css('.fr-element')
  end

  scenario 'has dropdown when content attribute has options' do
    @scene.content_attributes.create(name: 'Content', options: 'erste Option, zweite Option')
    find('.add-button').trigger('click')
    page.find('#map').trigger('click')
    sleep(1)
    expect(page).to have_css('select.content-attribute-dropdown')
  end
end
