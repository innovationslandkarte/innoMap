feature 'Guest Contribution', :js do
  scenario 'no access on non public scene' do
    create_stack(:scene)
    visit graphical_scene_map_path(id: @scene.id, locale: :de)
    expect(page).to_not have_css('.add-button')
  end

  scenario 'on geo scene' do
    create_stack(:geo_scene)
    @scene.update!(public_contributions_allowed: true)
    visit geo_scene_map_path(id: @scene.id, locale: :de)
    guest_creates_info_button
  end

  scenario 'on graphic scene' do
    create_stack(:scene)
    @scene.update!(public_contributions_allowed: true)
    visit graphical_scene_map_path(id: @scene.id, locale: :de)
    guest_creates_info_button
  end

  def guest_creates_info_button
    find('.add-button').trigger('click')
    page.find('#map').trigger('click')
    sleep(0.5)
    fill_in('Kurztitel (max. 40 Zeichen)', with: 'Guest Info Button')
    click_on 'Speichern'
    sleep(0.5)
    find('.add-button').trigger('click')
    if find_all('.marker-cluster').any? # due to differing zoom level
      find_all('.marker-cluster')[1].trigger('click')
    end
    sleep(0.5)
    expect(page).to have_css('.map-button.not-confirmed', text: 'Guest Info Button')
    expect(InfoButton.find_by(text: 'Guest Info Button').visible).to eq(false)
  end
end
