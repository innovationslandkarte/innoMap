feature 'Info Button Editor', :js do
  before do
    @user = create(:user)
    create_stack(:geo_scene)
    @user.assign_permission_for(@scene.main_scene, edit: true)
    log_in
    visit geo_scene_map_path(id: @scene.id, locale: :de)
  end

  scenario 'creates info button' do
    find('.add-button').trigger('click')
    page.find('#map').trigger('click')
    sleep(1)
    fill_in('Kurztitel (max. 40 Zeichen)', with: 'New Info Button')
    click_on 'Speichern'
    sleep(1)
    find('.add-button').trigger('click')
    if find_all('.marker-cluster').any? # due to differing zoom level
      find_all('.marker-cluster')[1].trigger('click')
    end
    sleep(1)
    expect(page).to have_css('.map-button', text: 'New Info Button')
    expect(InfoButton.find_by(text: 'New Info Button')).to_not be_nil
  end

  scenario 'edits info button' do
    if find_all('.marker-cluster').any? # due to differing zoom level
      find_all('.marker-cluster').last.trigger('click')
    end
    find('.edit-button').trigger('click')
    find('.map-button').trigger('click')
    sleep(0.5)
    fill_in('Kurztitel (max. 40 Zeichen)', with: 'Edited Info Button')
    click_on 'Speichern'
    expect(page).to have_css('.map-button', text: 'Edited Info Button')
  end

  scenario 'deletes info button' do
    expect(@scene.info_buttons.count).to eq(2)
    if find_all('.marker-cluster').any? # due to differing zoom level
      find_all('.marker-cluster').first.trigger('click')
    end
    expect(page).to have_css('.map-button', text: 'Second Info Button')
    find('.delete-button').trigger('click')
    find('.map-button').trigger('click')
    sleep(1)
    expect(page).to_not have_css('.map-button', text: 'Second Info Button')
    expect(@scene.info_buttons.count).to eq(1)
  end
end
