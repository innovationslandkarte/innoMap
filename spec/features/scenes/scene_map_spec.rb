feature 'Scene Map', :js do
  before do
    create_stack(:scene)
    create_system_effect_chain
  end

  scenario 'has link to list view' do
    visit graphical_scene_map_path(id: @scene.id, locale: :de)
    find('.icon-th-list').trigger('click')
    sleep(1)
    expect(page).to have_content('Info Button')
    expect(page).to_not have_css('#map')
  end

  scenario 'shows info buttons immediately when only one sub scene' do
    visit graphical_scene_map_path(id: @scene.id, locale: :de)
    expect(page).to_not have_css('.map-button', text: 'First Scene')
    expect(page).to_not have_css('.map-button', text: 'First Sub Scene')
    expect(page).to have_css('.map-button', text: 'First Info Button')
  end

  scenario 'shows sub scenes when more than one existing' do
    create(
      :sub_scene,
      text: 'Second Sub Scene',
      latitude: 120,
      longitude: 35,
      scene_id: @scene.id,
    )
    visit graphical_scene_map_path(id: @scene.id, locale: :de)
    expect(page).to_not have_css('.map-button', text: 'First Scene')
    expect(page).to_not have_css('.map-button', text: 'First Info Button')
    expect(page).to have_css('.map-button', text: 'First Sub Scene')
    expect(page).to have_css('.map-button', text: 'Second Sub Scene')
    find('.map-button', text: 'First Sub Scene').trigger('click')
    expect(page).to_not have_css('.map-button', text: 'First Sub Scene')
    expect(page).to have_css('.map-button', text: 'First Info Button')
  end

  scenario 'has working category button with image overlay toggle' do
    visit graphical_scene_map_path(id: @scene.id, locale: :de)
    # base image + overlay + white_layer
    expect(page).to have_css('.leaflet-image-layer', count: 3)
    find('.category-toggle').trigger('click')
    expect(page).to have_css('.leaflet-image-layer', count: 1)
    find('.category-toggle').trigger('click')
    expect(page).to have_css('.leaflet-image-layer', count: 3)
  end

  scenario 'shows base overlay' do
    @scene.update(base_image_overlay: open(Rails.root + 'app/assets/images/seed/world.jpeg'))
    visit graphical_scene_map_path(id: @scene.id, locale: :de)
    find('.category-toggle').trigger('click') # hides category overlay
    expect(page).to have_css('.leaflet-image-layer', count: 2)
  end

  scenario 'has working system effect buttons and index rows' do
    visit graphical_scene_map_path(id: @scene.id, locale: :de)
    expect(page).to_not have_content('System Effect Info')
    find('.system-effect-button', text: 1).trigger('click')
    expect(page).to have_content('First System Effect')
    expect(page).to have_content('System Effect Info')
    find('.system-effect-button', text: 2).trigger('click')
    expect(page).to have_content('Second System Effect')
    expect(page).to_not have_content('First System Effect Info')
  end

  scenario 'does not shows welcome modal if none exists' do
    visit graphical_scene_map_path(id: @scene.id, locale: :de)
    expect(page).to_not have_css('#welcome-modal')
  end

  scenario 'shows welcome modal if welcome text exists' do
    @scene.update(welcome: 'Any fancy welcome text.')
    visit graphical_scene_map_path(id: @scene.id, locale: :de)
    expect(page).to have_css('#welcome-modal')
    expect(page).to have_content('Any fancy welcome text.')
  end

  scenario 'back buttons are dynamic' do
    skip 'please write a test!!!'
  end

  scenario 'has working filter buttons' do
    visit graphical_scene_map_path(id: @scene.id, locale: :de)

    find('.text-filter-toggle', visible: false).trigger('click')
    fill_in('feature-search', visible: false, with: 'Second')
    expect(page).to_not have_css('.map-button', text: 'First Info Button')

    fill_in('feature-search', visible: false, with: 'First')
    expect(page).to have_css('.map-button', text: 'First Info Button')

    fill_in('feature-search', visible: false, with: 'First')
    find('.category-toggle').trigger('click')
    expect(page).to_not have_css('.map-button', text: 'First Info Button')
  end
end
