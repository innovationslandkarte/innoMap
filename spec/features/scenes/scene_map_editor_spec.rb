feature 'Scene Map Editor', :js do
  before do
    @main_scenes = [
      create(:main_scene, text: 'Jupiter'),
      create(:main_scene, text: 'Mars'),
    ]
    @user = create(:user)
    create_stack(:scene)
    create(
      :sub_scene,
      text: 'Second Sub Scene',
      latitude: 120,
      longitude: 35,
      scene_id: @scene.id,
    )

    @user.assign_permission_for(@main_scene, edit: true)

    visit root_path
    log_in
    visit graphical_scene_map_path(id: @scene.id, locale: :de)
  end

  scenario 'edits sub scenes' do
    find('.edit-button').trigger('click')
    find('.map-button', text: 'First Sub Scene').trigger('click')
    expect(page).to have_css('.btn', text: 'SAVE')
    fill_in('Name', with: 'Edited Sub Scene')
    find('.btn', text: 'SAVE').trigger('click')
    sleep(0.1)
    expect(page).to_not have_css('.map-button', text: 'First Sub Scene')
    expect(page).to have_css('.map-button', text: 'Edited Sub Scene')
  end

  scenario 'shows validation in form' do
    find('.edit-button').trigger('click')
    find('.map-button', text: 'First Sub Scene').trigger('click')
    expect(page).to have_css('.btn', text: 'SAVE')
    fill_in('Name', with: '')
    find('.btn', text: 'SAVE').trigger('click')
    sleep(0.1)
    expect(page).to have_content('SAVE')
    expect(page).to have_content('Text ist zu kurz')
  end

  scenario 'creates new sub scenes' do
    find('.add-button').trigger('click')
    page.find('#map').trigger('click')
    expect(page).to have_css('.btn', text: 'SAVE')
    fill_in('Name', with: 'New Sub Scene')
    find('.btn', text: 'SAVE').trigger('click')
    sleep(0.1)
    expect(page).to have_css('.map-button', text: 'New Sub Scene')
  end

  scenario 'deletes sub scenes' do
    find('.delete-button').trigger('click')
    find('.map-button', text: 'First Sub Scene').trigger('click')
    expect(page).to_not have_css('.map-button', text: 'First Sub Scene')
    expect(SubScene.count).to eq(1)
  end

  scenario 'allows info buttons to be made invisible' do
    skip 'bottom part has to be overworked'
    find('.category-toggle').trigger('click')
    find('.map-button', text: 'First Sub Scene').trigger('click')
    expect(page).to have_css('.map-button', text: 'First Info Button')
    expect(page).to_not have_css('.map-button.not-confirmed', text: 'First Info Button')
    find('.edit-button').trigger('click')
    find('.map-button', text: 'First Info Button').trigger('click')
    sleep(0.5)
    uncheck 'info_button_visible'
    find('.btn', text: 'SPEICHERN').trigger('click')
    sleep(0.5)
    expect(page).to have_css('.map-button.not-confirmed', text: 'First Info Button')
    find('.edit-scene').trigger('click')
    expect(page).to have_css('.transparent')
    expect(page).to have_content('First Info Button')
    # do not show info button on list for guests
    log_out
    visit scene_list_path(id: @scene.id)
    expect(page).to_not have_content('First Info Button')
  end

  scenario 'creates new category marker' do
    find('.map-button', text: 'First Sub Scene').trigger('click')
    expect(page).to_not have_css('.image-icon')
    expect(page).to have_css('.add-category-marker-button')
    find('.add-category-marker-button').trigger('click')
    page.find('#map').trigger('click')
    expect(page).to have_css('.image-icon')
  end

  scenario 'clones info buttons' do
    find('.map-button', text: 'First Sub Scene').trigger('click')
    find('.clone-button').trigger('click')
    expect(page).to have_css('.info_button_clone')
    find_all('.info_button_clone').first.trigger('click')
    find('#map').trigger('click')
    expect(page).to_not have_css('.info_button_clone')
    expect(page).to have_css('.info-button', text: 'First Info Button', count: 2)
  end
end
