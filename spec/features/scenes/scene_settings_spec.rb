feature 'Scene Settings', :js do
  before do
    @user = create(:user)
    create_stack(:scene)

    @user.assign_permission_for(@main_scene, edit: true)

    visit root_path
    log_in
    visit graphical_scene_settings_path(id: @scene.id, locale: :de)
  end

  scenario 'deletes scene' do
    click_on 'Config'
    click_on 'Delete map'
    expect(Scene.count).to eq(0)
  end

  scenario 'creates scene categories' do
    click_link 'Categories'
    expect(page).to_not have_content 'Second Category'
    click_on 'Assign a category to this map'
    sleep(0.5)
    fill_in('scene_category_name', with: 'Second Category')
    fill_in('scene_category_color', with: '#000000')
    page.find('.btn', text: 'SAVE').trigger('click')
    sleep(0.5)
    click_on 'Categories'
    sleep(0.5)
    expect(page).to have_content 'Second Category'
  end

  scenario 'deletes scene categories' do
    click_link 'Categories'
    expect(page).to have_content 'First Category'
    find('.icon-trash-1').trigger('click')
    sleep(0.5)
    click_link 'Categories'
    expect(page).to_not have_content 'First Category'
  end

  scenario 'creates system effects' do
    click_link 'System Effects'
    expect(page).to_not have_content 'First System Effect Chain'
    expect(page).to_not have_content 'First System Effect'
    click_on 'Create new system effect chain'
    sleep(0.5)
    select 'First Category', from: 'system_effect_chain_scene_category_id'
    find('#system_effect_chain_text').set('First System Effect Chain')
    system_effect_id = '#system_effect_chain_system_effects_attributes_0_'
    find(system_effect_id + 'text').set('First System Effect')
    find(system_effect_id + 'position').set(1)
    find(system_effect_id + 'longitude').set(10)
    find(system_effect_id + 'latitude').set(20)
    find('.btn', text: 'SAVE').trigger('click')
    sleep(0.5)
    click_on 'System Effects'
    expect(page).to have_content 'First System Effect Chain'
    expect(page).to have_content 'First System Effect'
  end
end
