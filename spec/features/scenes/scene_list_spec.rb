feature 'Scene View', :js do
  before do
    @user = create(:user)

    @main_scene = create(:main_scene)
    @scene = create(:scene, main_scene_id: @main_scene.id)
    @sub_scene = create(:sub_scene, scene_id: @scene.id)
    @scene_category = create(:scene_category, scene: @scene)
    @info_button = create(:info_button, sub_scene_id: @sub_scene.id, scene_category_id: @scene_category.id)

    @user.assign_permission_for(@main_scene, edit: true)

    visit root_path
    log_in
    visit graphical_scene_list_path(id: @scene.id, locale: :de)
  end

  scenario 'has link to map view' do
    find('.icon-map').trigger('click')
    sleep(1)
    expect(page).to_not have_content('CONTENT OF FIRST SCENE')
    expect(page).to have_css('#map')
    expect(page).to have_content('First Scene')
  end

  scenario 'updates subscene' do
    find('.edit-sub-scene').trigger('click')
    sleep(0.1)
    fill_in('Name', with: 'Edited Sub Scene')
    find('.btn', text: 'SAVE').trigger('click')
    sleep(0.1)
    expect(page).to_not have_content 'First Sub Scene'
    expect(page).to have_content 'Edited Sub Scene'
  end

  scenario 'shows validation' do
    find('.edit-sub-scene').trigger('click')
    sleep(0.1)
    fill_in('Name', with: '')
    find('.btn', text: 'SAVE').trigger('click')
    sleep(0.1)
    expect(page).to have_content 'First Sub Scene'
    expect(page).to have_content 'Text ist zu kurz'
  end
end
