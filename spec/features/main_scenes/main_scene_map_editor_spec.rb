feature 'Main Scene Map Editor', :js do
  before do
    @user = create(:user)
    create_stack(:scene)
    @user.assign_permission_for(@main_scene, edit: true)

    @scene.id
    log_in
    visit main_scene_map_path(id: @main_scene.id, locale: :de)
  end

  scenario 'edits scenes' do
    find('.edit-button').trigger('click')
    find('.map-button', text: 'First Scene').trigger('click')
    expect(page).to have_css('.btn', text: 'SCHLIESSEN')
    fill_in('Name', with: 'Edited Scene')
    find('.btn', text: 'SPEICHERN').trigger('click')
    sleep(2)
    # expect(page).to_not have_css('.map-button', text: 'First Scene')
    expect(page).to have_css('.map-button', text: 'Edited Scene')
  end

  scenario 'creates new graphical scenes' do
    find('.add-button').trigger('click')
    page.find('#map').trigger('click')
    page.find('#select-graphical-map').trigger('click')

    expect(page).to have_css('.btn', text: 'SPEICHERN')
    fill_in('Name', with: 'New Scene')
    fill_in('graphical_scene_longitude', with: 10) # has actually not to be set, but done here to get proper map positioning
    fill_in('graphical_scene_latitude', with: 10)
    fill_in('graphical_scene_content_attributes_attributes_0_name', with: 'foo')
    page.attach_file(
      'graphical_scene_base_image',
      Rails.root + 'app/assets/images/seed/base.png'
    ).click # workaround due to a bug -> https://github.com/teampoltergeist/poltergeist/issues/866
    find('.btn', text: 'SPEICHERN').trigger('click')
    expect(page).to have_css('.map-button', text: 'New Scene')
    new_scene = Scene.find_by(text: 'New Scene')
    expect(new_scene.content_attributes.map(&:name)).to contain_exactly('foo')
  end

  scenario 'creates new geo scenes' do
    find('.add-button').trigger('click')
    page.find('#map').trigger('click')
    page.find('#select-geo-map').trigger('click')

    expect(page).to have_css('.btn', text: 'SAVE')
    fill_in('Name', with: 'New Geo Scene')
    fill_in('geo_scene_longitude', with: 10) # has actually not to be set, but done here to get proper map positioning
    fill_in('geo_scene_latitude', with: 10)
    fill_in('geo_scene_content_attributes_attributes_0_name', with: 'foo')
    find('.btn', text: 'SAVE').trigger('click')
    sleep 0.5
    expect(page).to have_css('.map-button', text: 'New Geo Scene')
    new_scene = Scene.find_by(text: 'New Geo Scene')
    expect(new_scene.content_attributes.map(&:name)).to contain_exactly('foo')
  end

  scenario 'deletes scenes' do
    find('.delete-button').trigger('click')
    find('.map-button', text: 'First Scene').trigger('click')
    expect(page).to_not have_css('.map-button', text: 'First Scene')
    expect(Scene.count).to eq(0)
  end
end
