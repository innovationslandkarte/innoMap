feature 'Main Scene Map', :js do
  before do
    @main_scenes = [
      create(:main_scene, text: 'Jupiter'),
      create(:main_scene, text: 'Mars'),
    ]
    create_stack(:scene)
  end

  scenario 'is linked on main scenes view' do
    visit '/de/maps'
    expect(page).to have_css('h4', text: 'First Main Scene')
    expect(page).to have_css('h4', text: 'Jupiter')
    expect(page).to have_css('h4', text: 'Mars')
    find_link(href: main_scene_map_path(id: @main_scene, locale: :de)).trigger('click')
    expect(page).to have_css('.map-button', text: 'First Scene')
  end

  scenario 'has working category button with image overlay toggle' do
    create(:main_scene_category, main_scene_id: @main_scene.id)
    visit main_scene_map_path(id: @main_scene.id, locale: :de)

    # base image + overlay + white_layer
    expect(page).to have_css('.leaflet-image-layer', count: 3)
    find('.category-toggle').trigger('click')
    expect(page).to have_css('.leaflet-image-layer', count: 1)
    find('.category-toggle').trigger('click')
    expect(page).to have_css('.leaflet-image-layer', count: 3)
  end

  scenario 'works with category without image overlay' do
    skip 'loads overlay image with empty src'
    create(:main_scene_category, category_id: @category.id, main_scene_id: @main_scene.id, image: nil)
    visit main_scene_map_path(id: @main_scene.id, locale: :de)

    expect(page).to have_css('.leaflet-image-layer', count: 1)
    find('.category-toggle').trigger('click')
    expect(page).to have_css('.leaflet-image-layer', count: 1)
  end

  scenario 'shows welcome modal if welcome text exists' do
    @main_scene.update(welcome: 'Any fancy welcome text.')
    visit main_scene_map_path(id: @main_scene.id, locale: :de)
    expect(page).to have_css('#welcome-modal')
    expect(page).to have_content('Any fancy welcome text.')
  end

  scenario 'does not show welcome modal if none exists' do
    visit main_scene_map_path(id: @main_scene.id, locale: :de)
    expect(page).to_not have_css('#welcome-modal')
  end
end
