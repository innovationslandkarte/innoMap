feature 'Main Scene View', :js do
  before do
    @user = create(:user)
    @main_scene = create(:main_scene)
    @scene1 = create(:scene, text: 'First Scene', main_scene_id: @main_scene.id)
    @scene2 = create(:scene, text: 'Second Scene', main_scene_id: @main_scene.id)

    @user.assign_permission_for(@main_scene, edit: true, own: true)

    visit root_path
    log_in
    visit main_scene_settings_path(id: @main_scene.id, locale: :de)
  end

  scenario 'shows scenes' do
    [@scene1, @scene2].each do |s|
      expect(page).to have_css('a', text: s.text)
    end
  end

  scenario 'deletes main scene' do
    sleep(1)
    click_on('Delete map')
    expect(MainScene.count).to eq(0)
  end

  scenario 'links to empty scene view' do
    click_on 'First Scene'
    expect(page).to have_content 'First Scene'
    expect(page).to have_content 'Delete map'
  end
end
