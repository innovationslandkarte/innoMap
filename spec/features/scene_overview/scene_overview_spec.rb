feature 'Scene Overview', :js do
  before do
    @user = create(:user)
    log_in
    visit scene_overview_path
  end

  scenario 'creates main scene and gives user admin rights for created maps' do
    find('#new-main-scene').trigger('click')
    sleep(1)
    fill_in('Name', with: 'Main scene map')
    page.attach_file(
      'main_scene_base_image',
      File.join(Rails.root + 'app/assets/images/seed/base.png')
    ).click # workaround due to a bug -> https://github.com/teampoltergeist/poltergeist/issues/866
    find('.btn', text: 'SAVE').trigger('click')
    sleep(0.5)
    expect(page).to have_css('h4', text: 'Main scene map')
    find('.edit-scene').trigger('click')
    expect(page).to have_content('Change configuration')
  end
end
