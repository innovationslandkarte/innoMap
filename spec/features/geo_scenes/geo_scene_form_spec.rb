feature 'Geo Scene Form', :js do
  before do
    @user = create(:user)
    create_stack(:geo_scene)

    @user.assign_permission_for(@scene.main_scene, edit: true)

    visit root_path
    log_in
    visit geo_scene_settings_path(id: @scene.id, locale: :de)

    click_on 'Config'
    click_on 'Change configuration'
    sleep(0.5)
  end

  scenario 'deletes content attribute' do
    fill_in('geo_scene_content_attributes_attributes_0_name', with: '')
    find('.btn', text: 'SAVE').trigger('click')
    sleep(1)

    expect(@scene.reload.content_attributes.count).to eq(0)
  end

  scenario 'creates content attribute' do
    fill_in('geo_scene_content_attributes_attributes_0_name', with: 'Attribute')
    fill_in('geo_scene_content_attributes_attributes_0_options', with: 'erste Option zweite Option')
    find('.btn', text: 'SAVE').trigger('click')
    sleep(1)

    expect(@scene.reload.content_attributes.first.name).to eq('Attribute')
    expect(@scene.reload.content_attributes.first.options).to eq('erste Option zweite Option')
  end

  scenario 'does not create equally named content attributes' do
    fill_in('geo_scene_content_attributes_attributes_0_name', with: 'Attribute')
    fill_in('geo_scene_content_attributes_attributes_1_name', with: 'Attribute!')
    find('.btn', text: 'SAVE').trigger('click')
    expect(page).to have_content('Please use unique content attribute names')
  end
end
