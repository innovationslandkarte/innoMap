feature 'Geo Scene Map', :js do
  before do
    create_stack(:geo_scene)
  end

  scenario 'has link to list view' do
    visit geo_scene_map_path(id: @scene.id, locale: :de)
    find('.icon-th-list').trigger('click')
    sleep(1)
    expect(page).to have_content('Info Button')
    expect(page).to_not have_css('#map')
  end

  scenario 'shows info buttons immediately when only one sub scene and has working category buttons' do
    visit geo_scene_map_path(id: @scene.id, locale: :de)
    expect(page).to_not have_css('.map-button', text: 'First Scene')
    expect(page).to_not have_css('.map-button', text: 'First Sub Scene')
    find_all('.marker-cluster').last.trigger('click')
    expect(page).to have_css('.map-button', text: 'First Info Button')
    find('.category-toggle').trigger('click')
    expect(page).to_not have_css('.map-button', text: 'First Info Button')
  end

  scenario 'shows sub scenes when more than one existing' do
    create(
      :sub_scene,
      text: 'Second Sub Scene',
      latitude: 120,
      longitude: 35,
      scene_id: @scene.id,
    )
    visit geo_scene_map_path(id: @scene.id, locale: :de)
    expect(page).to_not have_css('.map-button', text: 'First Scene')
    expect(page).to_not have_css('.map-button', text: 'First Info Button')
    expect(page).to have_css('.map-button', text: 'First Sub Scene')
    expect(page).to have_css('.map-button', text: 'Second Sub Scene')
    find('.map-button', text: 'First Sub Scene').trigger('click')
    sleep(0.5)
    expect(page).to_not have_css('.map-button', text: 'First Sub Scene')
    expect(page).to have_css('.marker-cluster')
  end

  scenario 'does not show elements from graphical scene with same id' do
    create(
      :sub_scene,
      text: 'Second Sub Scene',
      latitude: 120,
      longitude: 35,
      scene_id: @scene.id,
    )
    visit geo_scene_map_path(id: @scene.id, locale: :de)
    expect(page).to have_css('.map-button', text: 'First Sub Scene')
    expect(all('.map-button').count).to eq(2)
  end

  scenario 'shows welcome modal if welcome text exists' do
    @scene.update(welcome: 'Any fancy welcome text.')
    visit geo_scene_map_path(id: @scene.id, locale: :de)
    expect(page).to have_css('#welcome-modal')
    expect(page).to have_content('Any fancy welcome text.')
  end

  scenario 'does not show welcome modal if none exists' do
    visit geo_scene_map_path(id: @scene.id, locale: :de)
    expect(page).to_not have_css('#welcome-modal')
  end

  scenario 'does have boundary layer toggle' do
    visit geo_scene_map_path(id: @scene.id, locale: :de)
    10.times do
      find('.zoom-out').click
      sleep(0.1)
    end
    expect(page).to_not have_css('.toggle-boundaries')
    3.times do
      find('.zoom-in').click
      sleep(0.1)
    end
    expect(page).to have_css('.toggle-boundaries')
    expect(page).to have_css('.leaflet-layer', count: 1)
    find('.toggle-boundaries').click
    expect(page).to have_css('.leaflet-layer', count: 2)
  end

  scenario 'does cluster points' do
    3.times do
      create(
        :info_button,
        latitude: 10,
        longitude: 10,
        sub_scene_id: @sub_scene.id,
        scene_category_id: @scene_category.id
      )
    end
    5.times do
      create(
        :info_button,
        latitude: 20,
        longitude: 20,
        sub_scene_id: @sub_scene.id,
        scene_category_id: @scene_category.id
      )
    end
    visit geo_scene_map_path(id: @scene.id, locale: :de)
    expect(page).to have_css('.cluster-pie-label', text: 5)
    expect(page).to have_css('.cluster-pie-label', text: 3)
  end

  scenario 'does show cluster marker on landing and text button when marker clicked' do
    @info_button = create(:info_button, sub_scene_id: @sub_scene.id, scene_category_id: @scene_category.id)
    visit geo_scene_map_path(id: @scene.id, locale: :de)
    expect(page).to_not have_content('Info Button')
    find_all('.marker-cluster').first.trigger('click')
    sleep(0.5)
    expect(page).to_not have_css('.marker-cluster')
    expect(page).to have_content('Info Button')
  end
end
