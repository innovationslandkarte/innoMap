feature 'Scene Map Editor', :js do
  before do
    @user = create(:user)
    create_stack(:geo_scene)
    create(
      :sub_scene,
      text: 'Second Sub Scene',
      latitude: 120,
      longitude: 35,
      scene_id: @scene.id,
    )
    @user.assign_permission_for(@scene.main_scene, edit: true)
    visit root_path
    log_in
    visit geo_scene_map_path(id: @scene.id, locale: :de)
  end

  scenario 'edits sub scenes' do
    find('.edit-button').trigger('click')
    find('.map-button', text: 'First Sub Scene').trigger('click')
    expect(page).to have_css('.btn', text: 'SAVE')
    fill_in('Name', with: 'Edited Sub Scene')
    find('.btn', text: 'SAVE').trigger('click')
    sleep(0.1)
    expect(page).to_not have_css('.map-button', text: 'First Sub Scene')
    expect(page).to have_css('.map-button', text: 'Edited Sub Scene')
  end

  scenario 'shows validation in form' do
    find('.edit-button').trigger('click')
    find('.map-button', text: 'First Sub Scene').trigger('click')
    expect(page).to have_css('.btn', text: 'SAVE')
    fill_in('Name', with: '')
    find('.btn', text: 'SAVE').trigger('click')
    sleep(0.1)
    expect(page).to have_content('SAVE')
    expect(page).to have_content('Text ist zu kurz')
  end

  scenario 'creates new sub scenes' do
    find('.add-button').trigger('click')
    page.find('#map').trigger('click')
    expect(page).to have_css('.btn', text: 'SAVE')
    fill_in('Name', with: 'New Sub Scene')
    find('.btn', text: 'SAVE').trigger('click')
    sleep(0.1)
    expect(page).to have_css('.map-button', text: 'New Sub Scene')
  end

  scenario 'deletes sub scenes' do
    find('.delete-button').trigger('click')
    find('.map-button', text: 'First Sub Scene').trigger('click')
    expect(page).to_not have_css('.map-button', text: 'First Sub Scene')
    expect(SubScene.count).to eq(1)
  end

  scenario 'clones info buttons' do
    find('.map-button', text: 'First Sub Scene').trigger('click')
    find('.clone-button').trigger('click')
    expect(page).to have_css('.info_button_clone')
    find_all('.info_button_clone').first.trigger('click')
    find('#map').trigger('click')
    expect(page).to_not have_css('.info_button_clone')
    expect(page).to have_css('.leaflet-marker-icon', count: 3)
  end
end
