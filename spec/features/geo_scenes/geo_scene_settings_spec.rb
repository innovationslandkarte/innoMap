feature 'Geo Settings', :js do
  before do
    @user = create(:user)
    create_stack(:geo_scene)

    @user.assign_permission_for(@scene.main_scene, edit: true)

    visit root_path
    log_in
    visit geo_scene_settings_path(id: @scene.id, locale: :de)
  end

  scenario 'deletes scene' do
    click_on 'Config'
    click_on 'Delete map'
    expect(GeoScene.count).to eq(0)
  end

  scenario 'creates scene categories' do
    click_on 'Categories'
    expect(page).to_not have_content 'Second Category'
    click_on 'Assign a category to this map'
    sleep(0.5)
    fill_in('scene_category_name', with: 'Second Category')
    fill_in('scene_category_color', with: '#000000')
    page.find('.btn', text: 'SAVE').trigger('click')
    sleep(0.5)
    click_on 'Categories'
    sleep(0.5)
    expect(page).to have_content 'Second Category'
  end

  scenario 'deletes scene categories' do
    click_on 'Categories'
    expect(page).to have_content 'First Category'
    find('.icon-trash-1').trigger('click')
    sleep(0.5)
    click_on 'Categories'
    expect(page).to_not have_content 'First Category'
  end
end
