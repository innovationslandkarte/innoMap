feature 'Geo Scene View', :js do
  before do
    @user = create(:user)
    create_stack(:geo_scene)

    @user.assign_permission_for(@scene.main_scene, edit: true)

    visit root_path
    log_in
    visit geo_scene_list_path(id: @scene.id, locale: :de)
  end

  scenario 'has datatable with info buttons and filter fields' do
    expect(page).to have_css('tr', count: 3) # header table row included
    find_all('.icon-pencil')[1].trigger('click')
    sleep(1)
    fill_in('info_button_ContentAttribute', with: 'This is a new content')
    expect(page).to_not have_content('CONTENT OF THE WORLD')
    click_on 'Speichern'
    fill_in('Content_Attribute', with: 'This is a new content')

    expect(page).to have_css('tr', count: 2) # header table row included
  end

  scenario 'has link to map view' do
    find('.icon-map').trigger('click')
    sleep(1)

    expect(page).to_not have_content('CONTENT OF THE WORLD')
    expect(page).to have_css('#map')
    expect(page).to have_content('The World')
  end

  scenario 'updates sub scene' do
    find('.edit-sub-scene').trigger('click')
    sleep(0.1)
    fill_in('Name', with: 'Edited Sub Scene')
    page.find('.btn', text: 'SAVE').trigger('click')

    expect(page).to have_content 'Edited Sub Scene'
    expect(page).to_not have_content 'First Sub Scene'
  end
end
