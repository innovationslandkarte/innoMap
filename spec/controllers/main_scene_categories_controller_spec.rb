describe MainSceneCategoriesController do
  let(:main_scene) { create(:main_scene, text: 'First Main Scene') }

  before do
    request.env['HTTP_REFERER'] = 'where_i_came_from'
  end

  describe 'MainSceneCategoriesController' do
    it 'creates main scene category when user has rights' do
      start_session
      @user.assign_permission_for(main_scene, edit: true)
      params = {
        main_scene_id: main_scene.id,
        name: 'Category',
        color: '#000000',
        image: image,
      }
      expect { post :create, main_scene_category: params }.to change(MainSceneCategory, :count).by(1)
    end

    it 'does not create main scene category when not logged in' do
      params = {
        main_scene_id: main_scene.id,
        name: 'Category',
        color: '#000000',
        image: image,
      }
      expect { post :create, main_scene_category: params }.to change(MainSceneCategory, :count).by(0)
    end
  end

  private

  def image
    ActionDispatch::Http::UploadedFile.new(
      filename: 'world.jpeg',
      type: 'image/jpeg',
      tempfile: File.new(Rails.root + 'app/assets/images/seed/world.jpeg')
    )
  end
end
