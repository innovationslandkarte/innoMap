describe GeoScenesController do
  let(:geo_scene_1) { create(:geo_scene) }

  describe 'GET show_on_map' do
    it 'has a 200 status code' do
      get :show_on_map, id: geo_scene_1.id
      expect(response.status).to eq(200)
    end

    it 'provides variables' do
      get :show_on_map, id: geo_scene_1.id
      expect(assigns[:geo_scene]).to eq(geo_scene_1)
    end
  end
end
