describe MainScenesController do
  let(:main_scene_1) { create(:main_scene, text: 'First Main Scene') }
  let(:main_scene_2) { create(:main_scene, text: 'Second Main Scene') }

  describe 'GET index' do
    it 'has a 200 status code' do
      get :show_on_map, id: main_scene_1.id
      expect(response.status).to eq(200)
      expect(assigns[:main_scene]).to eq(main_scene_1)
    end
  end
end
