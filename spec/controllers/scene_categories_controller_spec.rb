describe SceneCategoriesController do
  let(:main_scene) { create(:main_scene, text: 'First Main Scene') }
  let(:scene) { create(:scene, main_scene_id: main_scene.id) }

  before do
    request.env['HTTP_REFERER'] = 'where_i_came_from'
  end

  describe 'SceneCategoriesController' do
    it 'creates scene category when user has rights' do
      start_session
      @user.assign_permission_for(main_scene, edit: true)
      params = {
        name: 'Category',
        color: '#000000',
        scene_id: scene.id,
        image: image,
      }
      expect { post :create, scene_category: params }.to change(SceneCategory, :count).by(1)
    end

    it 'does not create scene category when not logged in' do
      params = {
        name: 'Category',
        color: '#000000',
        scene_id: scene.id,
        image: image,
      }
      expect { post :create, scene_category: params }.to change(SceneCategory, :count).by(0)
    end
  end

  private

  def image
    ActionDispatch::Http::UploadedFile.new(
      filename: 'world.jpeg',
      type: 'image/jpeg',
      tempfile: File.new(Rails.root + 'app/assets/images/seed/world.jpeg')
    )
  end
end
