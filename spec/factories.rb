FactoryGirl.define do
  factory :info_button do
    text 'First Info Button'
    latitude { rand(60..65) }
    longitude { rand(60..65) }
    content_store { {content: "<strong>Foo:</strong><br>BVG - Berliner Verkehrsgesellschaft</p><p>ifjisjifjdijfiigfh<strong>bar:  </strong><br>dsfkfsodkdsfkifjdushufhsduudsfh </p><p>"} }
    sub_scene
    scene_category
  end

  factory :geo_scene do
    main_scene
    text 'The World'
  end

  factory :scene do
    text 'First Scene'
    latitude { rand(60..63) }
    longitude { rand(60..63) }
    base_image do
      File.open(File.join(Rails.root + 'app/assets/images/seed/base.png'))
    end
  end

  factory :system_effect_chain do
    text 'First System Effect Chain'
  end

  factory :system_effect do
    text 'First System Effect'
    latitude { rand(55..57) }
    longitude { rand(50..52) }
    position 1
  end

  factory :sub_scene do
    text 'First Sub Scene'
    latitude { rand(62..64) }
    longitude { rand(62..64) }
  end

  factory :main_scene do
    text 'First Main Scene'
    base_image do
      File.open(File.join(Rails.root + 'app/assets/images/seed/world.jpeg'))
    end
  end

  factory :main_scene_category do
    name 'First Category'
    color '#ed2121'
    icon do
      File.open(File.join(Rails.root + 'app/assets/images/seed/icon.png'))
    end
    image do
      File.open(File.join(Rails.root + 'app/assets/images/seed/05.png'))
    end
  end

  factory :scene_category do
    name 'First Category'
    color '#ed2121'
    icon do
      File.open(File.join(Rails.root + 'app/assets/images/seed/icon.png'))
    end
    image do
      File.open(File.join(Rails.root + 'app/assets/images/seed/05.png'))
    end
  end

  factory :user do
    email 'user@test.com'
    password 'secret123'
  end

  factory :content_attribute do
    name 'Content Attribute'
  end
end
