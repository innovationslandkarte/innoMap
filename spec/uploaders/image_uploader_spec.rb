RSpec.describe ImageUploader do
	include CarrierWave::Test::Matchers

  let(:attachment) { Attachment.new }
  let(:uploader) { ImageUploader.new(attachment, :image) }

	before do
		ImageUploader.enable_processing = true
	end

  after do
    ImageUploader.enable_processing = false
    uploader.remove!
  end
  
  context 'Whitelisted formats' do
    it 'can upload svg' do
      skip 'To be implemented'
      File.open(path_to_svg) { |f| uploader.store!(f) }
      
      expect(uploader).to be_format('svg') || be_format('mvg') # RMagick: SVG -> MVG
    end

    it 'can upload png' do
      File.open(path_to_png) { |f| uploader.store!(f) }

      expect(uploader).to be_format('png')
    end

    it 'can upload jpeg' do
      File.open(path_to_jpg) { |f| uploader.store!(f) }

      expect(uploader).to be_format('jpeg')
    end

    it 'can upload gif' do
      File.open(path_to_gif) { |f| uploader.store!(f) }

      expect(uploader).to be_format('gif')
    end

    it 'cannot upload other format' do
      expect {
        File.open(path_to_pdf) { |f| uploader.store!(f) }
      }.to raise_error(CarrierWave::IntegrityError)
    end

    it 'cannot upload fake formatted files' do
      skip 'Implement proper MIME type validation'

      expect {
        File.open(path_to_fake_png) { |f| uploader.store!(f) }
      }.to raise_error(CarrierWave::IntegrityError)
    end
  end

  private

  def path_to_gif
    "spec/support/images/test.gif"
  end

  def path_to_jpg
    "spec/support/images/test.jpg"
  end

  # def path_to_svg
  #   "spec/support/images/test.svg"
  # end

  def path_to_png
    "spec/support/images/test.png"
  end

  def path_to_pdf
    "spec/support/images/test.pdf"
  end

  def path_to_fake_png
    "spec/support/images/fake_png.png" # Is actually a PDF!
  end
end
