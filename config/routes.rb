# rubocop:disable Metrics/BlockLength
require_relative 'subdomains'

Rails.application.routes.draw do
  scope '(:locale)', locale: /de|en/ do
    get 'automated_driving' => 'geo_scenes#show_on_map', id: 1

    root 'pages#index'
    get 'maps', to: 'pages#scene_overview', as: 'scene_overview'
    get 'use_cases', to: 'pages#use_cases', as: 'use_cases'
    # get 'privacy', to: 'pages#privacy', as: 'privacy'
    # get 'imprint', to: 'pages#imprint', as: 'imprint'

    resources :users

    resources :main_scenes, shallow: true do
      resources :main_scene_categories
      resources :graphical_scenes, controller: :graphical_scenes
      resources :geo_scenes, controller: :geo_scenes
      resources :users
    end

    resources :graphical_scenes, shallow: true do
      resources :scene_categories
      resources :sub_scenes do
        resources :info_buttons
      end
    end

    resources :geo_scenes, shallow: true do
      resources :scene_categories
      resources :sub_scenes do
        resources :info_buttons
      end
    end

    resources :main_scenes
    resources :graphical_scenes, controller: :graphical_scenes
    resources :geo_scenes, controller: :geo_scenes
    resources :sub_scenes
    resources :info_buttons
    resources :scene_categories
    resources :main_scene_categories
    resources :system_effect_chains
    resources :system_effects

    post 'info_buttons/create_category_marker', to: 'info_buttons#create_category_marker'
    post 'info_buttons/:id/clone', to: 'info_buttons#clone'

    get 'sub_scenes/:id/all_info_buttons', to: 'sub_scenes#all_info_buttons'

    post 'export/pdf', to: 'export#pdf'
    get 'export/pdf', to: 'export#pdf'
    get 'export/single_item_pdf', to: 'export#single_item_pdf'
    get 'export/excel',  to: 'export#excel', as: 'excel'

    get 'map/:id', to: 'main_scenes#show_on_map', as: 'main_scene_map'
    get 'map/:id/settings', to: 'main_scenes#settings', as: 'main_scene_settings'

    get 'sub/:id', to: 'graphical_scenes#show_on_map', as: 'graphical_scene_map'
    get 'sub/:id/settings', to: 'graphical_scenes#settings', as: 'graphical_scene_settings'
    get 'sub/:id/list', to: 'graphical_scenes#info_button_list', as: 'graphical_scene_list'

    get 'gmap/:id', to: 'geo_scenes#show_on_map', as: 'geo_scene_map'
    get 'gmap/:id/settings', to: 'geo_scenes#settings', as: 'geo_scene_settings'
    get 'gmap/:id/list', to: 'geo_scenes#info_button_list', as: 'geo_scene_list'
    get 'gmap/:id/changes', to: 'geo_scenes#changes', as: 'geo_scene_changes'

    get '/login' => 'sessions#new', as: 'login'
    post '/login' => 'sessions#create'
    get '/logout' => 'sessions#destroy'

    post '/users' => 'users#create'

    post 'attachment/upload' => 'attachments#upload'

    get '/log', to: 'admin#log'
  end
end
