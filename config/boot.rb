ENV['BUNDLE_GEMFILE'] ||= File.expand_path('../../Gemfile', __FILE__)

require 'bundler/setup' # Set up gems listed in the Gemfile.

# Allow connections from 0.0.0.0 instead of localhost 
# This is needed to serve connections inside a docker container with rails 4
require 'rails/commands/server' 
module Rails
  class Server
    alias :default_options_alias :default_options
    def default_options
      default_options_alias.merge!(Host: '0.0.0.0')
    end
  end
end
