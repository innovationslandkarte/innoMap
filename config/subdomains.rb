Rails.application.routes.draw do
  scope '(:locale)', locale: /de|en/ do
    GEO_SCENES = [
      ['ad', 1],
      [%w[scooter scootersharing], 17],
      ['giz', 35],
      ['giz2', 36],
      [%w[projekte projects innoz], 9],
      ['modavo', 18],
      ['ti', 35],
      ['sutp', 36],
      ['gpsm', 37],
      ['tumi', 38],
    ].freeze

    GEO_SCENES.each do |scene|
      constraints subdomain: scene[0] do
        get '' => 'geo_scenes#show_on_map', id: scene[1]
      end
    end

    constraints subdomain: '' do
      get '' => 'pages#index'
    end
  end
end
