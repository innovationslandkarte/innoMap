tmp_path = '/innomap/tmp'

listen(3000, backlog: 64)
pid File.join(tmp_path, 'pids', 'unicorn.pid')

stderr_path '/innomap/log/unicorn.log'
stdout_path '/innomap/log/unicorn.log'

worker_processes 2
preload_app true
