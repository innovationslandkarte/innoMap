require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

CONTRIBUTION_LOGGER = Logger.new('log/contributions.log')

module InnoMap
  class Application < Rails::Application
    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}')]
    config.i18n.default_locale = :de
    config.i18n.available_locales = %i[de en]

    config.active_record.raise_in_transactional_callbacks = true

    config.autoload_paths += Dir[Rails.root.join('app', 'models', '{*/}')]

    config.action_dispatch.default_headers = { 'X-Frame-Options' => 'ALLOWALL' }
  end
end
